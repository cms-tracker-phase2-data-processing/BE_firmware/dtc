import cocotb
from cocotb.triggers import FallingEdge, Timer
import numpy as np
from typing import List


CLOCK_TICKS = 200

async def generate_payload_clock(dut):
    for _ in range(CLOCK_TICKS):
        dut.clk.value = 0
        await Timer(1, units="ns")
        dut.clk.value = 1
        await Timer(1, units="ns")


@cocotb.test()
async def test_trigger(dut):
    dut.reset.value = 0
    dut.delay.value = 0
    
    await cocotb.start(generate_payload_clock(dut))
    for i in range(CLOCK_TICKS):
        await FallingEdge(dut.clk)
        
        if i < 20:
            dut.delay.value = 0
        elif i < 40:
            dut.delay.value = 1
        elif i < 60:
            dut.delay.value = 2
        elif i < 80:
            dut.delay.value = 3
        elif i < 100:
            dut.delay.value = 4
        elif i < 120:
            dut.delay.value = 5
        elif i < 140:
            dut.delay.value = 6
        elif i < 160:
            dut.delay.value = 7
        elif i < 180:
            dut.delay.value = 8
        
        
        if i % 20 == 10:
            dut.data_in.valid.value = 1
        else:
            dut.data_in.valid.value = 0
            
        dut._log.info("i = %03d | data_in.valid = %s | data_out.valid = %s", i, dut.data_in.valid.value, dut.data_out.valid.value)            

        if i == 11:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 32:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 53:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 74:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 95:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 116:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 137:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 158:
            assert dut.data_out.valid.value == 1, "Data out valid not set"
        if i == 179:
            assert dut.data_out.valid.value == 1, "Data out valid not set"