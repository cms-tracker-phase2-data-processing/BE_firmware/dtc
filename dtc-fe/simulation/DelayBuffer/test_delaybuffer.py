import os
from pathlib import Path

from cocotb.runner import get_runner


def test_my_design_runner():
    sim = os.getenv("SIM", "riviera")
    

    proj_path = Path(__file__).resolve().parent

    sources = [
        "./emp_data_types.vhd",
        "../../firmware/hdl/DelayBuffer.vhd",
    ]

    runner = get_runner(sim)
    runner.build(
        vhdl_sources=sources,
        hdl_toplevel="delaybuffer",
        build_args=[
            "-2008"
        ]
    )

    runner.test(
        hdl_toplevel="DelayBuffer", 
        test_module="DelayBuffer",
        test_args=[
        ]
    )


if __name__ == "__main__":
    test_my_design_runner()
