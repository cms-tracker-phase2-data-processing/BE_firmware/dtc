library IEEE;
library XPM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use XPM.VCOMPONENTS.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


entity L1DataExtractor is
    generic (
      module_type : string;
      bandwidth   : integer;
      cic_type    : string
    );
    port (
	    --- Input Ports ---
        clk                 : in std_logic;
        data_in             : in std_logic_vector(cNumberOfL1ELinks(selectIndexFromModuleType(module_type, bandwidth))-1 downto 0);
        daq_ipb_ctrl        : in std_logic_vector(31 downto 0);
        daq_fifo_read       : in tDaqFlag;
        --- Output Ports ---
        daq_ipb_status      : out std_logic_vector(31 downto 0);
        daq_ipb_record      : out std_logic_vector(31 downto 0);
        daq_ipb_data        : out std_logic_vector(31 downto 0);
        daq_fifo_data       : out tDaqData;
        daq_fifo_empty      : out tDaqFlag
    );
end L1DataExtractor;


architecture Behavioral of L1DataExtractor is


-- ----

signal to_event_fifo     : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );
signal from_event_fifo   : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );
signal full_event_fifo   : std_logic;
signal empty_event_fifo  : std_logic;
signal write_event_fifo  : std_logic;
signal read_event_fifo   : std_logic;

signal to_record_fifo    : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );
signal from_record_fifo  : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );
signal full_record_fifo  : std_logic;
signal empty_record_fifo : std_logic;
signal write_record_fifo : std_logic;
signal read_record_fifo  : std_logic;

signal to_data_fifo      : std_logic_vector(cL1DataFifoWidth - 1 downto 0) := ( others => '0' );
signal from_data_fifo    : std_logic_vector(cL1DataFifoWidth - 1 downto 0) := ( others => '0' );
signal full_data_fifo    : std_logic;
signal empty_data_fifo   : std_logic;
signal write_data_fifo   : std_logic;
signal read_data_fifo    : std_logic;

signal reset             : std_logic;
signal event_number      : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);

signal ctrl_reg1         : std_logic_vector(31 downto 0);
signal ctrl_reg2         : std_logic_vector(31 downto 0);

--daqpath module signals
signal full_daqpath_data_fifo   : std_logic;
signal full_daqpath_event_fifo  : std_logic;
signal full_daqpath_record_fifo : std_logic;

----


begin

  --==============================--
  -- Decode stream according to module type
  --==============================--

  --==============================--
  g2S : if module_type = "2S" generate
  --==============================--
    gCIC1 : if cic_type = "CIC1" generate
      --==============================--
      L1DataDecoder: entity work.L1DataDecoder(module2SCIC1)
      --==============================--
      generic map (
        module_type      => module_type,
        bandwidth        => bandwidth
    )  
      port map (
          --- Input Ports ---
          clk            => clk,
          data_in        => data_in,
          reset          => reset,
          --- Output Ports ---
          event_number   => event_number,
          data_fifo      => to_data_fifo,
          event_fifo     => to_event_fifo,
          record_fifo    => to_record_fifo,
          data_fifo_wr   => write_data_fifo,
          event_fifo_wr  => write_event_fifo,
          record_fifo_wr => write_record_fifo
      );
    end generate;
    gCIC2 : if cic_type = "CIC2" generate
      --==============================--
      L1DataDecoder: entity work.L1DataDecoder(module2SCIC2)
      --==============================--
      generic map (
        module_type      => module_type,
        bandwidth        => bandwidth
    )
      port map (
          --- Input Ports ---
          clk            => clk,
          data_in        => data_in,
          reset          => reset,
          --- Output Ports ---
          event_number   => event_number,
          data_fifo      => to_data_fifo,
          event_fifo     => to_event_fifo,
          record_fifo    => to_record_fifo,
          data_fifo_wr   => write_data_fifo,
          event_fifo_wr  => write_event_fifo,
          record_fifo_wr => write_record_fifo
      );
    end generate;
  end generate;
  --==============================--
  gPS : if module_type = "PS" generate
  --==============================--
    g5G : if bandwidth = 5 generate
      --==============================--
      L1DataDecoder: entity work.L1DataDecoder(modulePS5G)
      --==============================--
      generic map (
        module_type      => module_type,
        bandwidth        => bandwidth
    )
      port map (
          --- Input Ports ---
          clk            => clk,
          data_in        => data_in,
          reset          => reset,
          --- Output Ports ---
          event_number   => event_number,
          data_fifo      => to_data_fifo,
          event_fifo     => to_event_fifo,
          record_fifo    => to_record_fifo,
          data_fifo_wr   => write_data_fifo,
          event_fifo_wr  => write_event_fifo,
          record_fifo_wr => write_record_fifo
      );
    end generate;
    g10G : if bandwidth = 10 generate
      --==============================--
      L1DataDecoder: entity work.L1DataDecoder(modulePS10G)
      --==============================--
      generic map (
        module_type      => module_type,
        bandwidth        => bandwidth
    )
      port map (
          --- Input Ports ---
          clk            => clk,
          data_in        => data_in,
          reset          => reset,
          --- Output Ports ---
          event_number   => event_number,
          data_fifo      => to_data_fifo,
          event_fifo     => to_event_fifo,
          record_fifo    => to_record_fifo,
          data_fifo_wr   => write_data_fifo,
          event_fifo_wr  => write_event_fifo,
          record_fifo_wr => write_record_fifo
      );
    end generate;
  end generate;


  --==============================--
  -- IPBus control logic for debug FIFOs
  --==============================--
  
  ipbus_ctrl_latched: process (clk)
  
    variable ctrl_detect : std_logic_vector(31 downto 0);
  
  begin
    if rising_edge(clk) then
    
      ctrl_reg2 <= ctrl_reg1;
      ctrl_reg1 <= daq_ipb_ctrl;
      
      -- ensure reads/resets are clocked and only rising edge of ipbus signals initiate a read
      ctrl_detect := (not ctrl_reg2) and ctrl_reg1; 

      reset            <= '0';
      read_event_fifo  <= '0';
      read_record_fifo <= '0';
      read_data_fifo   <= '0';
  
      if ctrl_detect(0) = '1' then
        reset <= '1';
      end if;
   
      if ctrl_detect(1) = '1' then
        read_event_fifo  <= '1';
        read_record_fifo <= '1';        
      end if;
  
      if ctrl_detect(2) = '1' then
        read_data_fifo <= '1';
      end if;
      
    end if;
  end process;


  daq_ipb_status(3 downto 0)   <= '0' & empty_data_fifo & empty_event_fifo & empty_record_fifo;
  daq_ipb_status(7 downto 4)   <= '0' & full_data_fifo & full_event_fifo & full_record_fifo;
  daq_ipb_status(15 downto 8)  <= (others => '0');
  daq_ipb_status(31 downto 16) <= event_number;

  daq_ipb_record <= from_event_fifo & from_record_fifo;
  
  daq_ipb_data   <= from_data_fifo;
  

--=================================================================================================

-- Data FIFOs are 1k 32bit words FIFOs - implemented using xpm macros
-- Ctrl FIFOs are 512 32bit words FIFOs - implemented using xpm macros

debug_data_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 1024, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
      -- write port
      wr_clk => clk,
      din => to_data_fifo,
      wr_en => write_data_fifo,
      full => full_data_fifo,
      -- read port
      rd_clk => clk,          
      dout => from_data_fifo,
      rd_en => read_data_fifo,
      empty => empty_data_fifo,
      --Others
      sleep => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      wr_rst_busy => open,
      rd_rst_busy => open
     );
     
daqpath_data_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 1024, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 32, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 32, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       -- write port
       wr_clk => clk,
       din => to_data_fifo,
       wr_en => write_data_fifo,
       full => full_daqpath_data_fifo,
       -- read port
       rd_clk => clk,          
       dout => daq_fifo_data.data_word,
       rd_en => daq_fifo_read.data_word,
       empty => daq_fifo_empty.data_word,
       --Others
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
 debug_event_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 512, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 16, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 16, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       -- write port
       wr_clk => clk,
       din => to_event_fifo,
       wr_en => write_event_fifo,
       full => full_event_fifo,
       -- read port
       rd_clk => clk,          
       dout => from_event_fifo,
       rd_en => read_event_fifo,
       empty => empty_event_fifo,
       --Others
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
 daqpath_event_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 512, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 16, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 16, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       -- write port
       wr_clk => clk,
       din => to_event_fifo,
       wr_en => write_event_fifo,
       full => full_daqpath_event_fifo,
       -- read port
       rd_clk => clk,          
       dout => daq_fifo_data.event_id,
       rd_en => daq_fifo_read.event_id,
       empty => daq_fifo_empty.event_id,
       --Others
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
     
debug_record_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 512, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 16, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 16, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       -- write port
       wr_clk => clk,
       din => to_record_fifo,
       wr_en => write_record_fifo,
       full => full_record_fifo,
       -- read port
       rd_clk => clk,          
       dout => from_record_fifo,
       rd_en => read_record_fifo,
       empty => empty_record_fifo,
       --Others
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );
     
daqpath_record_fifo : xpm_fifo_async
    generic map (
      CDC_SYNC_STAGES => 2, -- DECIMAL
      DOUT_RESET_VALUE => "0", -- String
      ECC_MODE => "no_ecc", -- String
      FIFO_MEMORY_TYPE => "block", -- String
      FIFO_READ_LATENCY => 0, -- DECIMAL  --first word fall through
      FIFO_WRITE_DEPTH => 512, -- DECIMAL
      FULL_RESET_VALUE => 0, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1, -- DECIMAL
      READ_DATA_WIDTH => 16, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "0000", -- String
      WAKEUP_TIME => 0, -- DECIMAL
      WRITE_DATA_WIDTH => 16, -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1 -- DECIMAL
     )
     port map (
       rst => reset,
       -- write port
       wr_clk => clk,
       din => to_record_fifo,
       wr_en => write_record_fifo,
       full => full_daqpath_record_fifo,
       -- read port
       rd_clk => clk,          
       dout => daq_fifo_data.n_words,
       rd_en => daq_fifo_read.n_words,
       empty => daq_fifo_empty.n_words,
       --Others
       sleep => '0',
       injectdbiterr => '0',
       injectsbiterr => '0',
       wr_rst_busy => open,
       rd_rst_busy => open
     );   

end Behavioral;

