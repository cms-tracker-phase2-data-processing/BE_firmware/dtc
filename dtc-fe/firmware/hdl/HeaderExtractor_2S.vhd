----
-- Author: David Monk

-- Description file for HeaderExtractor entity.
-- This entity takes in a stream of words whose length is equal to the number of
-- e-links on the front-end module, along with a flag for the start of the boxcar.
-- The entity converts this stream into stubs, which are sent out as soon as
-- possible, meaning that they are not buffered until the end of the boxcar.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


architecture module2S of HeaderExtractor is
    signal cic_header : tCICHeader := ('0', (others => '0'), (others => '0'), (others => '0'));
    signal header_mode_signal : std_logic := '0';

begin

    header_out <= cic_header;
    header_mode <= header_mode_signal;

    pMain: process(clk)
    begin
        if rising_edge(clk) then
            -- Header Extraction
            if header_frame = 0 then
                cic_header <= ('0', (others => '0'), (others => '0'), (others => '0'));
                cic_header.CIC_conf <= frame(4);
                cic_header.status(8 downto 5) <= frame(3 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 1 then
                cic_header.status(4 downto 0) <= frame(4 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 2 then
                cic_header.bcid(11 downto 7) <= frame(4 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 3 then
                cic_header.bcid(6 downto 2) <= frame(4 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 4 then
                cic_header.bcid(1 downto 0) <= frame(4 downto 3);
                cic_header.stub_count(5 downto 3) <= frame(2 downto 0);
                header_mode_signal <= '0';
            elsif header_frame = 5 then
                cic_header.stub_count(2 downto 0) <= frame(4 downto 2);
                header_mode_signal <= '0';
            end if;
        end if;
    end process;

end module2S;
