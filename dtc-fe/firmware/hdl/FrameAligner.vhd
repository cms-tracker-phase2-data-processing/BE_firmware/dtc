library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.emp_data_types.all;
use work.module_constants.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;

entity FrameAligner is
    generic (
        index : integer;
        module_type : string
    );
    port (
        --- Input Ports ---
        clk_p    : in std_logic;
        data_in  : in lword  := LWORD_NULL;
        --- Output Ports ---
        data_out : out lword := LWORD_NULL
    );
end FrameAligner;

architecture Behavioral of FrameAligner is

    type tFrameArray is array(2 * cAlignmentDepth - 1 downto 0) of std_logic_vector(cNumberOfELinks(selectIndexFromModuleType(module_type)) - 1 downto 0);
    signal frame_buffer : tFrameArray                                := (others => (others => '0'));
    signal valid_buffer : std_logic_vector(cAlignmentDepth downto 0) := (others => '0');

begin
    pMain : process(clk_p)
    begin
        if rising_edge(clk_p) then
            if data_in.strobe = '1' then
                -- Buffer data and valid bit
                frame_buffer <= frame_buffer(frame_buffer'high - 1 downto 0) & data_in.data(cNumberOfELinks(selectIndexFromModuleType(module_type)) - 1 downto 0);
                valid_buffer <= valid_buffer(valid_buffer'high - 1 downto 0) & data_in.valid;

                -- Tap data from the buffers at the correct offset
                for j in 0 to cNumberOfELinks(selectIndexFromModuleType(module_type)) - 1 loop
                    data_out.data(cNumberOfELinks(selectIndexFromModuleType(module_type)) - 1 downto 0)(j) <= frame_buffer(cAlignmentDepth + link_offsets(index)(j))(j);
                end loop;
                data_out.valid <= valid_buffer(valid_buffer'high);

                -- Set strobe bit
                data_out.strobe <= '1';
            else
                data_out <= LWORD_NULL;
            end if;
        end if;
    end process pMain;

end Behavioral;
