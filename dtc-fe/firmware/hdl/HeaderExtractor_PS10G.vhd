----
-- Author: Chithra Kurup

-- Description file for HeaderExtractor entity.
-- This entity takes in a stream of words whose length is equal to the number of
-- e-links on the front-end module, along with a flag for the start of the boxcar.
-- The entity converts this stream into stubs, which are sent out as soon as
-- possible, meaning that they are not buffered until the end of the boxcar.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


architecture modulePS10G of HeaderExtractor is
    signal cic_header : tCICHeader := ('0', (others => '0'), (others => '0'), (others => '0'));
    signal header_mode_signal : std_logic := '0';

begin

    header_out <= cic_header;
    header_mode <= header_mode_signal;

    pMain: process(clk)
    begin
        if rising_edge(clk) then
            -- Header Extraction
            if header_frame = 0 then
                cic_header <= ('0', (others => '0'), (others => '0'), (others => '0'));
                cic_header.CIC_conf <= frame(11);
                cic_header.status <= frame(10 downto 2);
                cic_header.bcid(11 downto 10) <= frame(1 downto 0);
                header_mode_signal <= '1';
            elsif header_frame = 1 then
                cic_header.bcid(9 downto 0) <= frame(11 downto 2);
                cic_header.stub_count(5 downto 4) <= frame(1 downto 0);
                header_mode_signal <= '0';
            elsif header_frame = 2 then
                cic_header.stub_count(3 downto 0) <= frame(11 downto 8);
                header_mode_signal <= '0';
            end if;
        end if;
    end process;

end modulePS10G;

