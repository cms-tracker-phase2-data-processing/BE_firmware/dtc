library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package dtc_link_maps_func is

    type tDTCInputLinkConfig is record
        index       : integer;
        module_type : string;
        bandwidth   : integer;
        cic_type    : string;
    end record tDTCInputLinkConfig;

end package dtc_link_maps_func;

package body dtc_link_maps_func is
end package body dtc_link_maps_func;