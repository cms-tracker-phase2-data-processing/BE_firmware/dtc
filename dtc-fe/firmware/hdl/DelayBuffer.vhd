library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.emp_data_types.all;

entity DelayBuffer is
    generic (
        MAX_DELAY  : integer := 32  -- Maximum number of delay cycles available
    );
    port (
        clk      : in  std_logic;
        reset    : in  std_logic;  -- Asynchronous reset
        delay    : in  integer range 0 to MAX_DELAY;  -- Dynamic delay selection
        data_in  : in  lword;
        data_out : out lword
    );
end DelayBuffer;

architecture rtl of DelayBuffer is
    -- Array to hold intermediate values for each delay stage.
    signal shift_reg : ldata(0 to MAX_DELAY) := (others => LWORD_NULL);
begin
    process(clk, reset)
    begin
        if reset = '1' then
            -- Initialize all stages to zero on reset.
            shift_reg <= (others => LWORD_NULL);
        elsif rising_edge(clk) then
            -- Load the current input into stage 0.
            shift_reg(0) <= data_in;
            -- Propagate the data through all stages.
            for i in 1 to MAX_DELAY loop
                shift_reg(i) <= shift_reg(i-1);
            end loop;
        end if;
    end process;

    -- Select the delayed output using the 'delay' input port.
    data_out <= shift_reg(delay);
end rtl;
