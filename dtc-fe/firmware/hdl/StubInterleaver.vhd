----
-- Author: David Monk

-- Description file for StubInterleaver entity.
-- This entity takes two streams of stubs from the two StubExtractor instances
-- per link and combines the stubs into a single stream.
-- Note: the algorithm acts in such a way as to favour stub_in_0, meaning that
-- stubs from stub_in_1 may be out of sync in comparison. This will only be a
-- concern for the PS 10G module links.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.emp_data_types.all;


entity StubInterleaver is
    port (
        --- Input Ports ---
        clk       : in std_logic;
        stub_in_0 : in lword    := LWORD_NULL;
        stub_in_1 : in lword    := LWORD_NULL;
        --- Output Ports ---
        stub_out  : out lword   := LWORD_NULL
    );
end StubInterleaver;

architecture Behavioral of StubInterleaver is

    type tCacheArray is array(0 to 2) of lword;
    signal cache : tCacheArray := (others => LWORD_NULL);

begin
    pMain : process(clk)
    begin
        if rising_edge(clk) then
            -- Case 1: both stub 0 and stub 1 are valid
            if stub_in_0.valid = '1' and stub_in_1.valid = '1' then
                -- Output stub 0
                stub_out <= stub_in_0;
                -- Send stub 1 to cache, place in lowest index of array that is empty
                if cache(0) = LWORD_NULL then
                    cache(0) <= stub_in_1;
                else
                    if cache(1) = LWORD_NULL then
                        cache(1) <= stub_in_1;
                    else
                        cache(2) <= stub_in_1;
                    end if;
                end if;
            -- Case 2: stub 0 is valid and stub 1 is not
            elsif stub_in_0.valid = '1' and stub_in_1.valid = '0' then
                stub_out <= stub_in_0;
            -- Case 3: stub 1 is valid and stub 0 is not
            elsif stub_in_0.valid = '0' and stub_in_1.valid = '1' then
                stub_out <= stub_in_1;
            -- Case 4: neither stub is valid
            else
                -- If cache is empty, output a null word
                if cache(0) = LWORD_NULL then
                    stub_out <= LWORD_NULL;
                -- Else, output first word and shift all others down by one
                else
                    stub_out <= cache(0);
                    for i in 1 to 2 loop
                        cache(i-1) <= cache(i);
                    end loop;
                    cache(2) <= LWORD_NULL;
                end if;
            end if;
        end if;
    end process pMain;
end Behavioral;
