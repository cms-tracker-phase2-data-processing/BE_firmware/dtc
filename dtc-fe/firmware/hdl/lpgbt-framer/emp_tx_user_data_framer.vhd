-- Author : Kirika Uchida
-- Date   : 02/06/2021 

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;

-- data_i in this example from channel buffer.  (Not 100 % sure how it should be used)

--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0); 
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--  
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe is ignored.
-- data_i.data(31 downto 0) carry the data to be sent to gbt at user_data field.
-- The first data_i.data with data_i.start = '1' are user_data_o(31 downto 0), the next data are user_data_o(63 downto 32), the third data (17 downto 0) are user_data_o(79 downto 62). 
-- N_EC_SPARE is 0 for this example.

-- the user_data_o is in clk40_i 

entity emp_tx_user_data_framer is
	generic (
	         INDEX          : integer;
	         CHANNEL_INDEX  : integer := 0;
	    	 N_EC_SPARE     : integer range 0 to 16 := 0
	);
	port (
	    clk_i              : in std_logic;  --- for ipbus (31MHz)
	    rst_i              : in std_logic;  --- for ipbus
	    ipb_in             : in ipb_wbus;   --- ipbus data in (from PC)
	    ipb_out            : out ipb_rbus;  --- ipbus data out (to PC)
        clken_i            : in  std_logic:='0';		
        downlink_rdy_i     : in  std_logic;
        clk_p_i            : in  std_logic;
        data_i             : in  lword; 
        ec_spare_data_i    : in  std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        user_data_o        : out std_logic_vector(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 0) -- for GBT, data should be at (79 downto 0)
        );   
end emp_tx_user_data_framer;

architecture interface of emp_tx_user_data_framer is    


    signal lpgbt_tx_data : std_logic_vector(31 downto 0);

    
begin

	user_data_o(DOWNLINK_USERDATA_MAX_LENGTH - 1 downto 32) <= (others => '0') when clken_i = '1';
	user_data_o(31 downto 0)                                <= lpgbt_tx_data   when clken_i = '1';


	lpgbt_tx_data(31 downto 24) <= "110" & data_i.data(28) & data_i.data(24) & data_i.data(20) & data_i.data(16) & "1"; -- group 3: fast cic right

	lpgbt_tx_data(23 downto 16) <= "110" & data_i.data(12) & data_i.data(8) & data_i.data(4) & data_i.data(0) & "1";    -- group 2: fast left

	lpgbt_tx_data(15 downto 8 ) <= "110" & data_i.data(28) & data_i.data(24) & data_i.data(20) & data_i.data(16) & "1"; -- group 1: fast cic left

	lpgbt_tx_data(7  downto 0 ) <= "110" & data_i.data(12) & data_i.data(8) & data_i.data(4) & data_i.data(0) & "1";    -- group 0: fast right



end interface;
