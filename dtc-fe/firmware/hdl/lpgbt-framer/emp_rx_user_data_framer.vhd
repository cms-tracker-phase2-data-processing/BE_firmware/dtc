-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;
use work.module_constants.all;
use work.dtc_constants.all;
use work.dtc_link_maps.all;

entity emp_rx_user_data_framer is
    generic (
        INDEX             : integer;
        CHANNEL_INDEX     : integer range 0 to 3;
        N_EC_SPARE        : integer range 0 to 16 := 0
    );
    port (
        --- Input Ports ---
        clken_i                      : in  std_logic;
        clk_p_i                      : in  std_logic;
        clk40_i                      : in  std_logic;
        uplink_rdy_i                 : in  std_logic;
        user_data_i                  : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0);
        --- Ouput Ports ---
        data_o                       : out  lword;
        ec_spare_data_o              : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0);
        --- IPBus Ports ---
        clk_i                        : in std_logic;  --- for ipbus (31MHz)
        rst_i                        : in std_logic;  --- for ipbus
        ipb_in                       : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                      : out ipb_rbus   --- ipbus data out (to PC)

    );
end emp_rx_user_data_framer;

architecture interface of emp_rx_user_data_framer is

    function selectModuleTypeFromChannel(channel : in integer) return string is
        variable module_type : string(1 to 2) := "PS";
    begin
        for i in 0 to cDTCInputLinkMap'length - 1 loop
            if cDTCInputLinkMap(i).index = channel then
                module_type := cDTCInputLinkMap(i).module_type;
            end if;
        end loop;
        return module_type;
    end selectModuleTypeFromChannel;

    function selectDataRateFromChannel(channel : in integer) return integer is
        variable bandwidth : integer := 5;
    begin
        for i in 0 to cDTCInputLinkMap'length - 1 loop
            if cDTCInputLinkMap(i).index = channel then
                bandwidth := cDTCInputLinkMap(i).bandwidth;
            end if;
        end loop;
        return bandwidth;
    end selectDataRateFromChannel;

    function decrementIndex(index : in integer) return integer is
    begin
        if index = 0 then
            return CLOCK_RATIO - 1;
        else
            return index - 1;
        end if;
    end decrementIndex;

    constant module_type : string  := selectModuleTypeFromChannel(INDEX*4 + CHANNEL_INDEX);
    constant bandwidth   : integer := selectDataRateFromChannel(INDEX*4 + CHANNEL_INDEX);
    constant cModuleELinks : integer := cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth));
    constant cModuleL1ELinks : integer := cNumberOfL1ELinks(selectIndexFromModuleType(module_type, bandwidth));

    type tUnboundedLinkArray is array(integer range <>) of std_logic_vector(7 downto 0);
    subtype tLinkArray is tUnboundedLinkArray(cModuleELinks - 1 downto 0);
    constant NullLinkArray : tLinkArray := (others => (others => '0'));

    type tUnboundedMultiCICLinkArray is array(integer range <>) of tLinkArray;
    subtype tMultiCICLinkArray is tUnboundedMultiCICLinkArray(0 to cNumberOfCICs - 1);
    constant NullMultiCICLinkArray : tMultiCICLinkArray := (others => NullLinkArray);

    signal data_s       : lword              := LWORD_NULL;
    
    type tUnboundedL1ALinkArray is array(integer range <>) of std_logic_vector(7 downto 0);
    subtype tL1ALinkArray is tUnboundedL1ALinkArray(cModuleL1ELinks - 1 downto 0);
    type tUnboundedMultiCICL1ALinkArray is array(integer range <>) of tL1ALinkArray;
    subtype tMultiCICL1ALinkArray is tUnboundedMultiCICL1ALinkArray(0 to cNumberOfCICs - 1);

    signal l1a_data     : tMultiCICL1ALinkArray           := (others => (others => (others => '0')));

    signal stub_links   : tMultiCICLinkArray := NullMultiCICLinkArray;

begin


    pMain : process(clk_p_i)
        variable time_index : integer range 0 to CLOCK_RATIO - 1 := 0;
    begin
        if rising_edge(clk_p_i) then

            if (clken_i = '1') then
              time_index := 0;
            else
              time_index := decrementIndex(time_index);
            end if;

            if (time_index = 0) then

                for i in 0 to cNumberOfCICs - 1 loop
                  for j in 0 to cModuleELinks - 1 loop
                    for k in 7 downto 0 loop
                      if cWordWidth(selectIndexFromModuleType(module_type, bandwidth)) = 8 then
                        stub_links(i)(j)(k) <= user_data_i(link_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + k);
                      else
                        if j < 6 then
                          stub_links(i)(j)(k) <= user_data_i(link_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + 2*k + 1);
                        else 
                          stub_links(i)(j)(k) <= user_data_i(link_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + 2*k);
                        end if;
                      end if;
                    end loop;
                  end loop;
                  for j in 0 to cModuleL1ELinks - 1 loop
                      --l1a_data(i)(j)(7 downto 0) <= user_data_i(l1a_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + 7 downto l1a_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j));
                    for k in 7 downto 0 loop
                      if cWordWidth(selectIndexFromModuleType(module_type, bandwidth)) = 8 then
                        l1a_data(i)(j)(k) <= user_data_i(l1a_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + k);
                      else
                        l1a_data(i)(j)(k) <= user_data_i(l1a_indices(selectIndexFromModuleType(module_type, bandwidth))(i)(j) + 2*k + 1 - j);
                      end if;        
                    end loop;
                  end loop;
                end loop;

            end if;

            data_s <= LWORD_NULL;
            data_s.valid  <= '1';
            data_s.strobe <= '1';
            data_s.start  <= '1';
            data_s.data(62) <= uplink_rdy_i;

            if (uplink_rdy_i = '1' and time_index < 8) then
                -- Iterate over 2D arrays, sending a slice per clock cycle
                for i in 0 to cNumberOfCICs - 1 loop
                    data_s.data(32*i + 31) <= '1';
                    for j in 0 to cModuleELinks - 1 loop
                        data_s.data(32*i + cModuleELinks - 1 - j) <= stub_links(i)(j)(time_index);
                    end loop;
                    for j in 0 to cModuleL1ELinks - 1 loop
                        --- Stream L1A link ---
                        data_s.data(32*i + cModuleELinks + cModuleL1ELinks - 1 - j) <= l1a_data(i)(j)(time_index);
                    end loop;
                end loop;
            end if;

        end if;
    end process pMain;

    ec_spare_data_o(N_EC_SPARE * 2 - 1 downto 0) <= user_data_i(N_EC_SPARE * 2 - 1 downto 0);
    data_o <= data_s;

end interface;

