-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.ipbus.all;
use work.emp_project_decl.all;
use work.emp_data_framer_decl.all;


--	constant LWORD_WIDTH: integer := 64;
--	type lword is
--		record
--			data: std_logic_vector(LWORD_WIDTH - 1 downto 0);
--			valid: std_logic;
--			start: std_logic;
--			strobe: std_logic;
--		end record;
--
-- data_i in this example.  (Not 100 % sure how it should be used)
-- I defined the data_i.start = '1' for the first 32 bits of 40 MHz data.
-- data_i.valid = '1' when the data is there.
-- data_i.strobe = '1'.

-- user_data_i(111 downto 0) carry the data from gbt at user_data field.

-- user_data_i are latched at clken_i = '1'
-- latched data are assigned to data_o(31 downto 0) in three clocks.
-- N_EC_SPARE is 0 for this example, so ec_spare_data_i is not filling user_data_o.

--      the user_data_i is in clk_p_i domain with clken_i signal (single clock pulse in every 8 clock).

entity emp_rx_user_data_framer is
    generic ( INDEX             : integer;
              CHANNEL_INDEX     : integer range 0 to 3;
    	      N_EC_SPARE        : integer range 0 to 16 := 0
    );
   port (
        clk_i                        : in std_logic;  --- for ipbus (31MHz)
        rst_i                        : in std_logic;  --- for ipbus
        ipb_in                       : in ipb_wbus;   --- ipbus data in (from PC)
        ipb_out                      : out ipb_rbus;  --- ipbus data out (to PC)
        clk_p_i                      : in  std_logic;
        uplink_rdy_i                 : in  std_logic;
        clken_i                      : in  std_logic;
        user_data_i                  : in  std_logic_vector(UPLINK_USERDATA_MAX_LENGTH - 1 downto 0); -- For GBT, WIDE_BUS data are at (111 downto 0), GBT_FRAME data are at (79 downto 0)
        data_o                       : out  lword;
        ec_spare_data_o              : out std_logic_vector(N_MAX_EC_SPARE * 2 - 1 downto 0)
   );
end emp_rx_user_data_framer;

architecture interface of emp_rx_user_data_framer is

    signal data_s           : lword := LWORD_NULL;
    signal uplink_data : std_logic_vector(111 downto 0) := (others => '0');
    signal counter : integer range 0 to 7 := 0;
    signal data_enabled : std_logic := '0';

begin

    pMain : process(clk_p_i)
    begin
        if rising_edge(clk_p_i) then
            if uplink_rdy_i = '0' then
                data_s <= LWORD_NULL;
            else
                if clken_i = '1' then
                    data_enabled <= '1';
                    counter <= 0;
                    uplink_data <= user_data_i(111 downto 0);
                else
                    if data_enabled = '1' then
                        if counter = 1 then
                            data_enabled <= '0';
                            counter <= 0;
                        else
                            counter <= counter + 1;
                        end if;
                        if counter = 0 then
                            data_s.start <= '1';
                        else
                            data_s.start <= '0';
                        end if;
                        data_s.strobe <= '1';
                        data_s.valid <= '1';
                        data_s.data(56 - 1 downto 0) <= uplink_data(56*(counter + 1) - 1 downto 56*counter);
                    else
                        data_s <= LWORD_NULL;
                    end if;
                end if;
            end if;
        end if;
    end process pMain;


    data_o <= data_s;
    ec_spare_data_o(N_EC_SPARE * 2 - 1 downto 0) <= user_data_i(N_EC_SPARE * 2 - 1 downto 0);
end interface;
