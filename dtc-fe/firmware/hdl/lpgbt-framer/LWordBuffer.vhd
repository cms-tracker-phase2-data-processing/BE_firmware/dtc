library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
  ---
use work.emp_data_types.all;

entity LWordBuffer is 
    generic (
        delay : integer range 2 to 64
    );
    port (
        --- Input Ports ---
        clk   : in  std_logic;
        d     : in  lword;
        --- Output Ports ---
        q     : out lword
    );
end entity LWordBuffer;


architecture rtl of LWordBuffer is

    constant depth : integer := delay - 2;

    signal sr : ldata(depth downto 0) := (others => LWORD_NULL);
  
begin

    process(clk)
    begin
        if rising_edge(clk) then
    
            sr(depth downto 1) <= sr(depth-1 downto 0);
            sr(0)              <= d;
      
            q <= sr(depth);
      
        end if;
    end process;
  
end architecture rtl;
