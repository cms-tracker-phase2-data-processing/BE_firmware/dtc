----
-- Author: David Monk

-- Description file for HeaderExtractor entity.
-- This entity takes in a stream of words whose length is equal to the number of
-- e-links on the front-end module, along with a flag for the start of the boxcar.
-- The entity converts this stream into stubs, which are sent out as soon as
-- possible, meaning that they are not buffered until the end of the boxcar.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.dtc_data_types.all;

entity HeaderExtractor is
    port (
        --- Input Ports ---
        clk          : in std_logic;
        header_frame : in integer;
        frame        : in std_logic_vector(35 downto 0) := (others => '0');
        --- Output Ports ---
        header_out   : out tCICHeader;
        header_mode  : out std_logic
    );
end HeaderExtractor;
