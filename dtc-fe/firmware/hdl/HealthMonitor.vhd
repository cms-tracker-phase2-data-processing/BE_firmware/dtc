----
-- Author: David Monk

-- Description file for HealthMonitor entity.

----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;


entity HealthMonitor is
    port (
        --- Input Ports ---
        clk_p                 : in std_logic;
        header_start          : in std_logic_vector(cNumberOfCICs - 1 downto 0);
        sync_loss             : in std_logic_vector(cNumberOfCICs - 1 downto 0);
        header_in             : in tCICHeaderArray(cNumberOfCICs - 1 downto 0);
        aligner_state         : in tAlignerArray(cNumberOfCICs - 1 downto 0);
        counter_reset         : in std_logic;
        uplink_rdy_i          : in std_logic;
        --- Output Ports ---

        --- IPBus Ports ---
        clk              : in  std_logic;
        rst              : in  std_logic;
        ipb_in           : in  ipb_wbus;
        ipb_out          : out ipb_rbus
    );
end HealthMonitor ;

architecture Behavorial of HealthMonitor is

    constant N_STAT                  : integer                                  := 14;
    signal status_registers          : ipb_reg_v(N_STAT - 1 downto 0)           := (others => (others => '0'));
    -- signal control_registers         : ipb_reg_v(5 downto 0)                    := (others => (others => '0'));

    signal header_reg                : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
    signal sync_loss_count_reg       : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
    signal stub_count_reg            : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
    signal cic_sync_error_count_reg  : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
    signal fe_error_flag_count_reg   : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
    signal cic_overflow_count_reg    : ipb_reg_v(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));

    signal aligner_status_reg        : ipb_reg_v(0 downto 0)                    := (others => (others => '0'));    
    signal uplink_not_rdy_count      : unsigned(31 downto 0)                    := (others => '0');
    signal uplink_rdy                : std_logic                                := '0';

    
    constant max_stubs :  unsigned(5 downto 0)                    := to_unsigned(16,6);

    --constant bin_width               : integer := 32;
    --signal stub_count_array          : ldata(cNumberOfCICs - 1 downto 0)        := (others => LWORD_NULL);
    --signal trigger_window            : std_logic_vector(36 - 1 downto 0)        := X"100000000";
    --signal histogram_reset           : std_logic                                := '0';
    --signal max_value0, max_value1    : std_logic_vector(bin_width - 1 downto 0) := (others => '0');

begin


    --==============================--
    fe_health_mon: entity work.ipbus_ctrlreg_v
    --==============================--
    generic map(
        N_CTRL       => 0,
        N_STAT       => N_STAT
    )
    port map(
        clk          => clk,
        reset        => rst,
        ipbus_in     => ipb_in,
        ipbus_out    => ipb_out,
        d            => status_registers
        -- q            => control_registers
    );

    aligner_status_reg(0)(8) <= uplink_rdy;
    
    status_registers(0 downto 0) <= aligner_status_reg;
    status_registers(2 downto 1) <= header_reg;
    status_registers(4 downto 3) <= sync_loss_count_reg;
    status_registers(6 downto 5) <= stub_count_reg;
    status_registers(8 downto 7) <= cic_sync_error_count_reg;
    status_registers(10 downto 9) <= fe_error_flag_count_reg;
    status_registers(12 downto 11) <= cic_overflow_count_reg;
    status_registers(13) <= std_logic_vector(uplink_not_rdy_count);
    

    --==============================--
    regInputs : process(clk_p)
    --==============================--
    begin
        if rising_edge(clk_p) then
            uplink_rdy <= uplink_rdy_i;
        end if;
    end process regInputs;

    --==============================--
    genRegisters : for i in 0 to cNumberOfCICs - 1 generate
    --==============================--
        signal sync_loss_count         : unsigned(31 downto 0) := (others => '0');
        signal stub_count              : unsigned(31 downto 0) := (others => '0');
        signal cic_sync_error_count    : unsigned(31 downto 0) := (others => '0');
        signal fe_error_flag_count     : unsigned(31 downto 0) := (others => '0');
        signal cic_overflow_count      : unsigned(31 downto 0) := (others => '0');

    begin
        header_reg(i)(27 downto 0)                <= cicHeaderToSLV(header_in(i));
        aligner_status_reg(0)(4*i + 3 downto 4*i) <= aligner_state(i);
        sync_loss_count_reg(i)                    <= std_logic_vector(sync_loss_count);
        stub_count_reg(i)                         <= std_logic_vector(stub_count);
        cic_sync_error_count_reg(i)               <= std_logic_vector(cic_sync_error_count);
        fe_error_flag_count_reg(i)                <= std_logic_vector(fe_error_flag_count);
        cic_overflow_count_reg(i)                 <= std_logic_vector(cic_overflow_count);

        --==============================--
        pCountSyncLossEvents : process(clk_p)
        --==============================--
        begin
            if rising_edge(clk_p) then
                if counter_reset = '1' then
                    sync_loss_count <= (others => '0');
                else
                    if sync_loss(i) = '1' and aligner_state(i)(2 downto 0) = "100" then
                        -- count occasions when aligner is in locked state, but a header is misaligned/out of sync
                        sync_loss_count <= sync_loss_count + 1;
                    end if;
                end if;
            end if;
        end process pCountSyncLossEvents; 

        --==============================--
        pCountStubEvents : process(clk_p)
        --==============================--
          variable n_stubs   :  unsigned(5 downto 0);
          variable cic_flag  :  std_logic;
          variable fe_flags  :  std_logic_vector(7 downto 0);

        begin
            if rising_edge(clk_p) then
                if counter_reset = '1' then
                    stub_count           <= (others => '0');
                    cic_sync_error_count <= (others => '0');
                    fe_error_flag_count  <= (others => '0');     
                    cic_overflow_count   <= (others => '0');                                        
                else
                    if header_start(i) = '1' and aligner_state(i) = "1100" then
                        -- only count when aligner is in locked state, and packet decoding is in sync

                        n_stubs := unsigned(header_in(i).stub_count);
                        cic_flag := header_in(i).status(0);
                        fe_flags := header_in(i).status(8 downto 1);

                        stub_count <= stub_count + n_stubs;

                        if cic_flag = '1' and fe_flags /= x"00" and n_stubs /= max_stubs then
                          cic_sync_error_count <= cic_sync_error_count + 1;
                        end if;
                        if cic_flag = '0' and fe_flags /= x"00" then
                          fe_error_flag_count <= fe_error_flag_count + 1;
                        end if;
                        if cic_flag = '1' and n_stubs = max_stubs then
                          cic_overflow_count <= cic_overflow_count + 1;
                        end if;

                    end if;
                end if;
            end if;
        end process pCountStubEvents;       
        
    end generate genRegisters;

    --==============================--
    pUplinkNotReadyCount : process(clk_p)
    --==============================--
    begin
        if rising_edge(clk_p) then
            if counter_reset = '1' then
                uplink_not_rdy_count <= (others => '0');
            else
                if uplink_rdy = '0' then
                    uplink_not_rdy_count <= uplink_not_rdy_count + 1;
                end if;
            end if;
        end if;
    end process pUplinkNotReadyCount;


end architecture Behavorial;
