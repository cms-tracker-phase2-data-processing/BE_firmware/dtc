----
-- Author: Chithra Kurup
-- Description file for HeaderFingerprintChecker entity



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;
use work.module_constants.all;


architecture modulePS5G of HeaderFingerprintChecker is

    type tRollingWindow is array(71 downto 0) of std_logic_vector(cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 0);
    signal rolling_window       : tRollingWindow       := (others => (others => '0'));
    signal match                : std_logic            := '0';

    constant cBCIDTapPoint      : integer := 0;
    constant cPaddingTapPoint   : integer := 4;
    constant cConfTapPoint      : integer := 3;

begin
    header_found <= match;

    pMain : process(clk)
        variable previous_bcid       : std_logic_vector(11 downto 0);
        variable bcid                : std_logic_vector(11 downto 0);
        variable fingerprint_match   : boolean;
        variable bcid_match          : boolean;
        variable conf_match          : boolean;
    begin
        if rising_edge(clk) and data_in.strobe = '1' then

            -- Update rolling window with new data
            rolling_window <= rolling_window(rolling_window'high - 1 downto 0) & data_in.data(cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 0);

            previous_bcid := rolling_window(cBCIDTapPoint + cBoxCarFrames + 2)(1 downto 0) & rolling_window(cBCIDTapPoint + cBoxCarFrames + 1) & rolling_window(cBCIDTapPoint + cBoxCarFrames)(5 downto 2);
            bcid := rolling_window(cBCIDTapPoint + 2)(1 downto 0) & rolling_window(cBCIDTapPoint + 1) & rolling_window(cBCIDTapPoint)(5 downto 2);

            -- Conditions for header
            fingerprint_match := rolling_window(cBoxCarFrames + cPaddingTapPoint) = b"000000" and rolling_window(cPaddingTapPoint) = b"000000";
            conf_match        := rolling_window(cBoxCarFrames + cConfTapPoint)(5) = '1' and rolling_window(cConfTapPoint)(5) = '1';

            if unsigned(bcid) > 7 then
              bcid_match := unsigned(bcid) - unsigned(previous_bcid) = 8;
            else
              bcid_match := unsigned(bcid) + 3564 - unsigned(previous_bcid) = 8;
            end if;
              
            if fingerprint_match and bcid_match and conf_match then
              match <= '1';
            else
              match <= '0';
            end if;
        end if;
    end process;
end architecture;

