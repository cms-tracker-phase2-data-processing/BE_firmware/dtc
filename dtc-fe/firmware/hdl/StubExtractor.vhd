----
-- Author: David Monk

-- Description file for StubExtractor entity.
-- This entity takes in a stream of words whose length is equal to the number of
-- e-links on the front-end module, along with a flag for the start of the boxcar.
-- The entity converts this stream into stubs, which are sent out as soon as
-- possible, meaning that they are not buffered until the end of the boxcar.
----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;

entity StubExtractor is
    generic (
        cic_index : integer;
        module_type : string;
        bandwidth : integer
    );
    port (
        --- Input Ports ---
        clk           : in std_logic;
        header_start  : in std_logic;
        data_in       : in lword                        := LWORD_NULL;
        aligner_state : in std_logic_vector(3 downto 0) := (others => '0');
        --- Output Ports ---
        stub_out      : out lword := LWORD_NULL;
        header_out    : out tCICHeader := ('0', (others => '0'), (others => '0'), (others => '0'))
    );
end StubExtractor;


architecture Behavorial of StubExtractor is

    -- The frame width, i.e. # elinks
    constant FRAME_WIDTH : integer := cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth));

    -- The output data width
    constant STUB_WIDTH   : integer := cStubWidth(selectIndexFromModuleType(module_type, bandwidth));
    constant HEADER_WIDTH : integer := cHeaderWidth;

    -- The stub bit offset (l->r) for the first frame containing stubs, 0 means stub starts at msb
    constant FIRST_STUB_OFFSET : integer := cFirstStubOffset(selectIndexFromModuleType(module_type, bandwidth));

    -- Specifies the width of the shifted word.
    constant SHIFT_REG_WIDTH : integer := FRAME_WIDTH + STUB_WIDTH;

    signal data_reg             : lword                                          := LWORD_NULL;

    signal frame, frame_shifted : std_logic_vector(SHIFT_REG_WIDTH - 1 downto 0) := (others => '0');
    signal buf_offset           : integer range 0 to SHIFT_REG_WIDTH - 1         := SHIFT_REG_WIDTH - FRAME_WIDTH;
    signal valid, header_mode   : std_logic                                      := '0';
    signal header_start1        : std_logic;
    signal header_frame         : integer range 0 to cHeaderFrames(selectIndexFromModuleType(module_type, bandwidth)) + 1           := 0;
    signal stub_o               : lword                                          := LWORD_NULL;
    signal cic_header           : tCICHeader                                     := ('0', (others => '0'), (others => '0'), (others => '0'));
    signal stub_counter         : integer                                        := 0;

begin

    ff: process(clk)
    begin
        if rising_edge(clk) then
            data_reg <= data_in;
        end if;
    end process;


    shift: process(clk)
        variable buf: std_logic_vector(SHIFT_REG_WIDTH - 1 downto 0) := (others => '0');
    begin
        if rising_edge(clk) then
            if data_reg.strobe = '1' then

                header_start1 <= header_start;

                -- Header Frame Counter
                if header_start1 = '1' then
                    header_frame <= 0;
                elsif header_frame = cHeaderFrames(selectIndexFromModuleType(module_type, bandwidth)) then
                    header_frame <= cHeaderFrames(selectIndexFromModuleType(module_type, bandwidth));
                    if aligner_state = "1100" then
                        header_out <= cic_header;
                    else
                        header_out <= ('0', (others => '0'), (others => '0'), (others => '0'));
                    end if;
                else
                    header_frame <= header_frame + 1;
                end if;

                -- Register Inputs
                frame <= std_logic_vector(to_unsigned(0, STUB_WIDTH)) & data_reg.data(FRAME_WIDTH-1 downto 0);

                -- Stub Extraction
                if header_mode = '1' then
                    -- reset buffer and pointers during header mode
                    frame_shifted <= (others => '0');
                    buf_offset <= STUB_WIDTH + FIRST_STUB_OFFSET;
                    buf := (others => '0');
                    valid <= '0';
                    stub_counter <= 0;
                else
                    -- determine if buffer word is full or not and shift offset accordingly
                    if buf_offset > FRAME_WIDTH then
                        -- buffer word not yet full
                        buf_offset <= buf_offset - FRAME_WIDTH;
                        valid <= '0';
                    else
                        -- full buffer word ready, shift pointer by stub word size, flag as valid
                        buf_offset <= buf_offset + STUB_WIDTH - FRAME_WIDTH;
                        valid <= '1';
                    end if;

                    -- shift frame so that it aligns with free space in buffer and merge with existing data
                    frame_shifted <= std_logic_vector(unsigned(frame) sll buf_offset);
                    buf := buf or frame_shifted;
                end if;


                -- Output Data
                stub_o <= LWORD_NULL;

                if valid = '1' then
                    if stub_counter < to_integer(unsigned(cic_header.stub_count)) then
                        stub_o.data <= std_logic_vector(to_unsigned(0, 42 - (HEADER_WIDTH + 1))) & std_logic_vector(to_unsigned(cic_index, 1)) & cicHeaderToSLV(cic_header) & std_logic_vector(to_unsigned(0, 22 - STUB_WIDTH)) & buf(SHIFT_REG_WIDTH - 1 downto FRAME_WIDTH);
                        stub_o.valid <= '1';
                        buf := buf(FRAME_WIDTH - 1 downto 0) & std_logic_vector(to_unsigned(0, STUB_WIDTH));
                        stub_counter <= stub_counter + 1;
                    end if;
                end if;
            else
                stub_o <= LWORD_NULL;
            end if;
        end if;
    end process;


    --==============================--
    -- Extract the header information
    --==============================--

    --==============================--
    g2S : if module_type = "2S" generate
    --==============================--
        signal wide_frame : std_logic_vector(35 downto 0) := (others => '0');
    begin

        wide_frame(SHIFT_REG_WIDTH - 1 downto 0) <= frame;

        --==============================--
        HeaderExtractor: entity work.HeaderExtractor(module2S)
        --==============================--
        port map (
            --- Input Ports ---
            clk => clk,
            header_frame => header_frame,
            frame => wide_frame,
            --- Output Ports ---
            header_out => cic_header,
            header_mode => header_mode
        );
    end generate;
    --==============================--
    gPS : if module_type = "PS" generate
    --==============================--
        g5G : if bandwidth = 5 generate
            signal wide_frame : std_logic_vector(35 downto 0) := (others => '0');
        begin

            wide_frame(SHIFT_REG_WIDTH - 1 downto 0) <= frame;

            --==============================--
            HeaderExtractor: entity work.HeaderExtractor(modulePS5G)
            --==============================--
            port map (
                --- Input Ports ---
                clk => clk,
                header_frame => header_frame,
                frame => wide_frame,
                --- Output Ports ---
                header_out => cic_header,
                header_mode => header_mode
            );
        end generate;
        g10G : if bandwidth = 10 generate
            signal wide_frame : std_logic_vector(35 downto 0) := (others => '0');
        begin

            wide_frame(SHIFT_REG_WIDTH - 1 downto 0) <= frame;

            --==============================--
            HeaderExtractor: entity work.HeaderExtractor(modulePS10G)
            --==============================--
            port map (
                --- Input Ports ---
                clk => clk,
                header_frame => header_frame,
                frame => wide_frame,
                --- Output Ports ---
                header_out => cic_header,
                header_mode => header_mode
            );
        end generate;
    end generate;

    stub_out <= stub_o when aligner_state = "1100" else LWORD_NULL;

end Behavorial;
