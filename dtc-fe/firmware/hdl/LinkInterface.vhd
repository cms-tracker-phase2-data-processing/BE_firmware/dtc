library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


use work.ipbus_decode_dtc_link_interface.all;


entity LinkInterface is
generic (
    module_type : string;
    bandwidth   : integer;
    cic_type    : string;
    enable_monitoring : boolean := true;
    emp_channel : integer := 0;
    delay_buffer_max : integer := 64
);
port (
    --- Input Ports ---
    clk_p            : in  std_logic;
    clk40            : in  std_logic;
    link_in          : in  lword := LWORD_NULL;
    daq_read         : in  tDaqFlagArray;
    global_fcmd      : in  tFastCommand;
    --- Output Ports ---
    link_out         : out lword := LWORD_NULL;
    stub_out         : out lword := LWORD_NULL;
    header_out       : out tCICHeaderArray(cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
    daq_out          : out tDaqDataArray;
    daq_empty        : out tDaqFlagArray;
    --- IPBus Ports ---
    clk              : in  std_logic;
    rst              : in  std_logic;
    ipb_in           : in  ipb_wbus;
    ipb_out          : out ipb_rbus;
    --- Debug Ports ---
    debug_header_start : out std_logic_vector(1 downto 0);
    debug_header_match : out std_logic_vector(1 downto 0);
    debug_aligner_state : out std_logic_vector(7 downto 0)
);
end LinkInterface;

architecture rtl of LinkInterface is


-- IPBus fabric

signal ipb_to_slaves            : ipb_wbus_array(N_SLAVES - 1 downto 0);
signal ipb_from_slaves          : ipb_rbus_array(N_SLAVES - 1 downto 0);

signal fe_control_registers     : ipb_reg_v(0 downto 0) := (others => (others => '0'));
signal fe_status_registers      : ipb_reg_v(0 downto 0) := (others => (others => '0'));

signal daq_status_registers     : ipb_reg_v(5 downto 0) := (others => (others => '0'));
signal daq_control_registers    : ipb_reg_v(1 downto 0) := (others => (others => '0'));


-- Link decoding and module readout

signal delayed_link_in          : lword                             := LWORD_NULL;
signal headers                  : tCICHeaderArray(cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
signal stubs                    : ldata(cNumberOfCICs - 1 downto 0) := (others => LWORD_NULL);
signal stubs_interleaved        : lword                             := LWORD_NULL;

signal aligner_state            : tAlignerArray(cNumberOfCICs - 1 downto 0)    := (others => (others => '0'));
signal header_start             : std_logic_vector(cNumberOfCICs - 1 downto 0) := (others => '0');
signal sync_loss                : std_logic_vector(cNumberOfCICs - 1 downto 0) := (others => '0');



begin


    header_out          <= headers;

    debug_header_start  <= header_start;
    debug_header_match  <= sync_loss;
    debug_aligner_state <= aligner_state(1) & aligner_state(0);
    fe_status_registers(0) <= std_logic_vector(to_unsigned(emp_channel, 32));


    --==============================--
    -- IPBus fabric
    --==============================--

    --==============================--
    fabric: entity work.ipbus_fabric_sel
    --==============================--
    generic map(
        NSLV            => N_SLAVES,
        SEL_WIDTH       => IPBUS_SEL_WIDTH
    )
    port map(
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_dtc_link_interface(ipb_in.ipb_addr),
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
    );

    --==============================--
    fe_control: entity work.ipbus_ctrlreg_v
    --==============================--
    generic map(
        N_CTRL            => 1,
        N_STAT            => 1
    )
    port map(
        clk               => clk,
        reset             => rst,
        ipbus_in          => ipb_to_slaves(N_SLV_FE_CTRL),
        ipbus_out         => ipb_from_slaves(N_SLV_FE_CTRL),
        d                 => fe_status_registers,
        q		          => fe_control_registers
    );


    --==============================--
    fe_daq: entity work.ipbus_ctrlreg_v
    --==============================--
    generic map(
        N_CTRL            => 2,
        N_STAT            => 6
    )
    port map(
        clk               => clk,
        reset             => rst,
        ipbus_in          => ipb_to_slaves(N_SLV_L1_DAQ),
        ipbus_out         => ipb_from_slaves(N_SLV_L1_DAQ),
        d                 => daq_status_registers,
        q		          => daq_control_registers
    );


    --==============================--
    LinkBufferInstance : entity work.DelayBuffer
    --==============================--
    generic map (
        MAX_DELAY => delay_buffer_max
    )
    port map (
        clk => clk_p,
        reset => fe_control_registers(0)(3),
        delay => to_integer(unsigned(fe_control_registers(0)(11 downto 4))),
        data_in => link_in,
        data_out => delayed_link_in
    );

    --==============================--
    -- Link decoding and module readout
    --==============================--


    --==============================--
    CicInterface: for i in 0 to cNumberOfCICs - 1 generate
    --==============================--

        signal stream_in         : lword       := LWORD_NULL;
        signal stream_in_aligned : lword       := LWORD_NULL;

        signal l1_data_in        : std_logic_vector(cNumberOfL1ELinks(selectIndexFromModuleType(module_type, bandwidth)) -1 downto 0)   := (Others=>'0'); 
        signal aligner_reset     : std_logic   := '0';
    
    begin

        stream_in.valid                              <= delayed_link_in.valid;
        stream_in.strobe                             <= delayed_link_in.data(32*i + 31);
        stream_in.data(cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 0) <= delayed_link_in.data(32*i + cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 32*i);
        
        l1_data_in                                   <= delayed_link_in.data(32*i + cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth)) + cNumberOfL1ELinks(selectIndexFromModuleType(module_type, bandwidth))-1 downto 32*i + cNumberOfELinks(selectIndexFromModuleType(module_type, bandwidth))); 
        aligner_reset                                <= fe_control_registers(0)(i);

        -- --==============================--
        -- FrameAlignerInstance : entity work.FrameAligner
        -- --==============================--
        -- generic map (
        --     index => i
        -- )
        -- port map (
        --     --- Input Ports ---
        --     clk_p => clk,
        --     data_in => stream_in,
        --     --- Output Ports ---
        --     data_out => stream_in_aligned
        -- );

        --==============================--
        HeaderAligner: entity work.HeaderAligner
        --==============================--
        generic map (
            module_type      => module_type,
            bandwidth        => bandwidth
        )
        port map(
            --- Input Ports ---
            clk              => clk_p,
            data_in          => stream_in,
            reset            => aligner_reset,
            --- Output Ports ---
            header_start     => header_start(i),
            state            => aligner_state(i),
            sync_loss        => sync_loss(i)
        );

        --==============================--
        StubExtractor: entity work.StubExtractor
        --==============================--
        generic map(
            cic_index        => i,
            module_type      => module_type,
            bandwidth        => bandwidth
        )
        port map(
            --- Input Ports ---
            clk              => clk_p,
            data_in          => stream_in,
            header_start     => header_start(i),
            aligner_state    => aligner_state(i),
            --- Output Ports ---
            stub_out         => stubs(i),
            header_out       => headers(i)
        );
        
        --==============================--
        L1DataExtractor: entity work.L1DataExtractor
        --==============================--
        generic map (
            module_type      => module_type,
            bandwidth        => bandwidth,
            cic_type         => cic_type
        )
        port map(
            --- Input Ports ---
            clk               => clk_p,
            data_in           => l1_data_in,
            daq_ipb_ctrl      => daq_control_registers(i),
            daq_fifo_read     => daq_read(i),      
            --- Output Ports ---
            daq_ipb_status    => daq_status_registers(3*i+0),
            daq_ipb_record    => daq_status_registers(3*i+1),
            daq_ipb_data      => daq_status_registers(3*i+2),
            daq_fifo_data     => daq_out(i),
            daq_fifo_empty    => daq_empty(i)
        );

    end generate CicInterface;


    --==============================--
    -- Stub interleaving
    --==============================--

    --==============================--
    StubInterleaver: entity work.StubInterleaver
    --==============================--
    port map(
        --- Input Ports ---
        clk           => clk_p,
        stub_in_0     => stubs(0),
        stub_in_1     => stubs(1),
        --- Output Ports ---
        stub_out      => stubs_interleaved
    );


    --==============================--
    -- Stub conversion for input to router
    --==============================--

    --==============================--
    StubConverter: entity work.StubConverter
    --==============================--
    generic map (
        module_type => module_type,
        bandwidth   => bandwidth
    )
    port map(
        --- Input Ports ---
        clk           => clk_p,
        stub_in       => stubs_interleaved,
        --- Output Ports ---
        stub_out      => stub_out
    );



    --==============================--
    -- Link encoding and module control
    --==============================--



    --==============================--
    LocalFastCommand: entity work.LocalFastCommand
    --==============================--
    port map(
        --- Input Ports ---
        clk_p            => clk_p,
        clk40            => clk40,
        global_fcmd      => global_fcmd,
        --- Output Ports ---
        data_out         => link_out,
        --- IPBus Ports ---
        clk              => clk,                       
        rst              => rst,                       
        ipb_in           => ipb_to_slaves(N_SLV_FAST_CMD),  
        ipb_out          => ipb_from_slaves(N_SLV_FAST_CMD)
    );



    --==============================--
    -- Health Monitoring
    --==============================--

    genMonitoring : if enable_monitoring = true generate

    begin

        --==============================--
        HealthMonitor: entity work.HealthMonitor
        --==============================--
        port map(
            --- Input Ports ---
            clk_p                 => clk_p,
            header_start          => header_start,
            sync_loss             => sync_loss,
            header_in             => headers,
            aligner_state         => aligner_state,
            counter_reset         => fe_control_registers(0)(2),
            uplink_rdy_i          => delayed_link_in.data(62),
            --- Output Ports ---

            --- IPBus Ports ---
            clk                   => clk,                       
            rst                   => rst,                       
            ipb_in                => ipb_to_slaves(N_SLV_HEALTH_MON),  
            ipb_out               => ipb_from_slaves(N_SLV_HEALTH_MON)
        );

    end generate genMonitoring;


end rtl;
