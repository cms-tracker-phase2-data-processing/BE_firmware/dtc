library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


architecture modulePS10G of L1DataDecoder is

type decoder_state is (idle, capture_armed, header_capture, payload_capture, store_event, error);

signal decoder         : decoder_state := idle;

signal header          : std_logic_vector(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth))+2 downto 0)   := ( others => '0' );  
signal payload_word    : std_logic_vector(cL1DataFifoWidth+2 downto 0) := ( others => '0' );
signal last_word       : std_logic_vector(cL1DataFifoWidth-1 downto 0) := ( others => '0' );

signal start_counter   : integer range 0 to 31 := cL1StartSequenceThresh;
signal header_counter  : integer range -1 to 35;
signal payload_counter : integer range -3 to 4500;
signal payload_sclust_cntr : integer;
signal payload_pclust_cntr : integer;
signal payload_sum : integer;
signal bit_counter     : integer range -1 to cL1DataFifoWidth - 1;
signal extra_word      : std_logic := '0';

signal event_counter   : unsigned(cL1CtrlFifoWidth - 1 downto 0) := to_unsigned(1, cL1CtrlFifoWidth);
-- DEBUG
signal sstatus     : std_logic_vector(cL1StatusWidth - 1 downto 0);
signal sl1a        : std_logic_vector(cL1IdWidth - 1 downto 0);
signal snPclusters : std_logic_vector(cL1NPcluster_width - 1 downto 0);
signal snSclusters : std_logic_vector(CL1NScluster_width - 1 downto 0);

----

-- decoder state machine monitors incoming L1A data line for CIC L1A packets:
--    CIC sends a sequence of 27 '1's, followed by a '0' to indicate the start of a packet
--    a packet contains a fixed length header depending on module/packet type (2S/PS)
--    the payload length also depends on content - the cluster width field indicates length

-- header and payload data are packed into words of width "cL1DataFifoWidth", and written to the data FIFO
-- when the packet ends, metadata is stored in two separate synchronised FWFT FIFOs:
--    event FIFO - the local event number (for synchronisation at the DTC/DAQ level),
--    record FIFO - a record of number of words written to the data FIFO for a given event

-- (at DTC or DAQ path/SLINK level, not implemented here)
-- DAQ firmware should check the FWFT L1A event number in every event FIFO against TCDS record
-- if ready to readout, number of words to read from RAM can be determined from record FIFO

-- also not yet implemented:
--   timeouts/error states when we are stuck in any non-idle state for too long
--   monitoring for CIC health using idle pattern? overall FE health to go in a separate block probably
--   different modes e.g. un/sparsified decoding for 2S modules, PS module decoding
--   how to handle 10G PS data (assuming two input bits, instead of one)

----

begin


  event_number <= std_logic_vector(event_counter-1);


  decoder_sm: process (clk)
  
    variable status     : std_logic_vector(cL1StatusWidth - 1 downto 0);
    variable l1a        : std_logic_vector(cL1IdWidth - 1 downto 0);
    variable nPclusters : std_logic_vector(cL1NPcluster_width - 1 downto 0);
    variable nSclusters : std_logic_vector(CL1NScluster_width - 1 downto 0);
  
    variable word_count : unsigned(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );

  begin
    if rising_edge(clk) then
    
      if reset = '1' then
      
        decoder        <= idle;
        start_counter  <= cL1StartSequenceThresh;
        event_counter  <= to_unsigned(1, cL1CtrlFifoWidth);
        
        event_fifo     <= ( others => '0' );
        record_fifo    <= ( others => '0' );
        data_fifo      <= ( others => '0' );
        
        data_fifo_wr   <= '0';
        event_fifo_wr  <= '0';
        record_fifo_wr <= '0';
        
      else
    
        data_fifo_wr   <= '0';
        event_fifo_wr  <= '0';
        record_fifo_wr <= '0';

        case decoder is
        
          -- wait for a start sequence of '1's indicating the start of a packet
          when idle =>
            -- idle pattern is a repeating 10101..
            if data_in = "11" then
              start_counter <= start_counter - 2;  -- 2 bits instead of 1 in 10G
            else
              start_counter <= cL1StartSequenceThresh;
            end if;
            if start_counter = 0 then
              -- if more than "cL1StartSequenceThresh" consecutive '1's, arm capture
              decoder <= capture_armed;
              start_counter <= cL1StartSequenceThresh;
            end if;

          -- wait for a single '0' indicating the start of header
          when capture_armed =>
            if data_in= "10" then  -- Header word starts with next two bits
              decoder <= header_capture;
              header_counter <= cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth));
              header(1 downto 0) <= data_in;
            elsif data_in(1) = '0' then  -- Header word already started
              decoder <= header_capture;
              header_counter <= cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth))-1;
              header(1 downto 0) <= data_in;
            end if;

          -- extract & store the header and calculate the payload length
          when header_capture =>
          
            -- shift in header
            if header_counter > 0 then
              header(1 downto 0) <= data_in;
              header(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth))+2 downto 2) <= header(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth)) downto 0);
              header_counter <= header_counter - 2;  -- 2 bits instead of 1
              
            elsif header_counter = 0 then  -- Aligned with the end of header, the two data in bits are for payload word
            
              decoder <= payload_capture;
              
              status := header(cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth)) + cL1StatusWidth-1 downto cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth)));
              l1a := header(cL1IdLow(selectIndexFromModuleType(module_type, bandwidth)) + cL1IdWidth-1 downto cL1IdLow(selectIndexFromModuleType(module_type, bandwidth)));

              -- nPclusters defines length of payload Pixels' data length
              nPclusters := header(cL1NPcluster_low + cL1NPcluster_width-1 downto cL1NPcluster_low);

              -- nSclusters defines length of payload strips' data length
              nSclusters := header(cL1NScluster_low + cL1NScluster_width-1 downto cL1NScluster_low);
              
              --debug
              snSclusters <= nSclusters;
              snPclusters <= nPclusters;
              sl1a <= l1a;
              sstatus <= status;
              
              -- store the header to the data FIFO, and set word count to 1 
              data_fifo <= l1a & status & nSclusters & nPclusters;
              data_fifo_wr <= '1';
              word_count := to_unsigned(1, cL1CtrlFifoWidth);
              
              bit_counter <= cL1DataFifoWidth - 2;
              payload_word(1 downto 0) <= data_in;
              payload_sclust_cntr <= to_integer(unsigned(nSclusters)) * cL1SClusterWidth;
              payload_pclust_cntr <= to_integer(unsigned(nPclusters)) * cL1PClusterWidth;
              payload_sum <= payload_sclust_cntr + payload_pclust_cntr;
              payload_counter <= payload_sum - 2;
              --payload_counter <= to_integer(unsigned(nSclusters))*cL1SClusterWidth + to_integer(unsigned(nPclusters))*cL1PClusterWidth - 2;
              
              
            else  --header counter = -1 (the two new bits and the header(0) bit belong to payload word)
              
              decoder <= payload_capture;
              
              status := header(cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth))+1 + cL1StatusWidth-1 downto cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth))+1);
              l1a := header(cL1IdLow(selectIndexFromModuleType(module_type, bandwidth))+1 + cL1IdWidth-1 downto cL1IdLow(selectIndexFromModuleType(module_type, bandwidth))+1);

              -- nPclusters defines length of payload Pixels' data length
              nPclusters := header(cL1NPcluster_low+1 + cL1NPcluster_width-1 downto cL1NPcluster_low+1);

              -- nSclusters defines length of payload strips' data length
              nSclusters := header(cL1NScluster_low+1 + cL1NScluster_width-1 downto cL1NScluster_low+1);
              
              --debug
              snSclusters <= nSclusters;
              snPclusters <= nPclusters;
              sl1a <= l1a;
              sstatus <= status;
              
              -- store the header to the data FIFO, and set word count to 1 
              data_fifo <= l1a & status & nSclusters & nPclusters;
              data_fifo_wr <= '1';
              word_count := to_unsigned(1, cL1CtrlFifoWidth);
              
              bit_counter <= cL1DataFifoWidth - 3;
              payload_word(2 downto 0) <= header(0) & data_in;
              payload_sclust_cntr <= to_integer(unsigned(nSclusters)) * cL1SClusterWidth;
              payload_pclust_cntr <= to_integer(unsigned(nPclusters)) * cL1PClusterWidth;
              payload_sum <= payload_sclust_cntr + payload_pclust_cntr;
              payload_counter <= payload_sum - 3;
              --payload_counter <= to_integer(unsigned(nSclusters))*cL1SClusterWidth + to_integer(unsigned(nPclusters))*cL1PClusterWidth - 3;
             
            end if;

          -- pack the payload into words until the predetermined packet length ends
          when payload_capture =>
            -- shift in payload
            payload_word(1 downto 0) <= data_in;
            payload_word(cL1DataFifoWidth+2 downto 2) <= payload_word(cL1DataFifoWidth downto 0);

            if payload_counter >0 then
            
              payload_counter <= payload_counter - 2;
              
              -- If buffer is full and all of the two previous bits belong to this data_fifo_word
              if bit_counter = 0 then
                data_fifo <= payload_word(cL1DataFifoWidth-1 downto 0);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                bit_counter <= cL1DataFifoWidth - 2;
                
              -- If buffer is full and one of the previous bits received is for the next data_fifo_word
              elsif bit_counter = -1 then 
                data_fifo <= payload_word(cL1DataFifoWidth downto 1);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                bit_counter <= cL1DataFifoWidth - 3;
                
              else
                bit_counter <= bit_counter - 2;
                
              end if;
            
            
            elsif payload_counter = 0 then  -- no bits remaining to store (all bits already in payload word buffer)
            
              decoder <= store_event;
            
              -- if word buffer still not full --- pad the word
              if bit_counter >= 0 then
                data_fifo <= std_logic_vector(unsigned(payload_word(cL1DataFifoWidth-1 downto 0)) sll bit_counter);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                extra_word <= '0';
                
              else  -- if word buffer full
                data_fifo <= payload_word(cL1DataFifoWidth downto 1);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                extra_word <= '1';
                last_word <= payload_word(0) & b"000" & X"0000000";
                
              end if;

            elsif payload_counter < -1 then -- payload_counter is -2 or -3 when Sclust = 0 and Pclust = 0 ie. no stub in the payload
              
              decoder <= store_event;
              extra_word <= '0';

            else   --payload_counter = -1, only one bit of the last two bits added to payload word is good
            
              decoder <= store_event;
              
              if bit_counter >= 0 then
                data_fifo <= std_logic_vector(unsigned(payload_word(cL1DataFifoWidth-1 downto 0)) sll bit_counter);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                extra_word <= '0';
                
              else  -- if word buffer full
                data_fifo <= payload_word(cL1DataFifoWidth downto 1);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
                extra_word <= '0';
                
              end if;
              
            end if;

          -- finish off packet handling by storing metadata to FIFOs, increment event counter, and reinitialise
          when store_event =>
            
            if (extra_word = '1') then
              data_fifo <= last_word;
              data_fifo_wr <= '1';
              word_count := word_count + 1;
            end if;
            
            event_fifo <= std_logic_vector(event_counter);
            record_fifo <= std_logic_vector(word_count);
            event_fifo_wr <= '1';
            record_fifo_wr <= '1';
            
            decoder <= idle;
            word_count := (others => '0');
            event_counter <= event_counter + 1;
            extra_word <= '0';
          
          when others =>
            decoder <= idle;
                                                    
        end case;

      end if;
    end if;
  end process;
  
  
  end modulePS10G;
