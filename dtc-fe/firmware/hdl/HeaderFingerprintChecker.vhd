----
-- Author: David Monk
-- Description file for HeaderFingerprintChecker entity



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;


entity HeaderFingerprintChecker is
    generic (
        module_type : string;
        bandwidth   : integer
    );
    port (
        --- Input Ports ---
        clk     : in std_logic;
        data_in : in lword     := LWORD_NULL;
        --- Output Ports ---
        header_found : out std_logic := '0'
    );
end HeaderFingerprintChecker;
