library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


architecture modulePS5G of L1DataDecoder is

type decoder_state is (idle, capture_armed, header_capture, payload_capture, store_event, error);

signal decoder         : decoder_state := idle;

signal header          : std_logic_vector(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 0)   := ( others => '0' );
signal payload_word    : std_logic_vector(cL1DataFifoWidth - 1 downto 0) := ( others => '0' );

signal start_counter   : integer range 0 to 31 := cL1StartSequenceThresh;
signal header_counter  : integer range 0 to 35;
signal payload_counter : integer range -1 to 4500;
signal bit_counter     : integer range 0 to cL1DataFifoWidth - 1;
signal data            : std_logic;

signal event_counter   : unsigned(cL1CtrlFifoWidth - 1 downto 0) := to_unsigned(1, cL1CtrlFifoWidth);


----

-- decoder state machine monitors incoming L1A data line for CIC L1A packets:
--    CIC sends a sequence of 27 '1's, followed by a '0' to indicate the start of a packet
--    a packet contains a fixed length header depending on module/packet type (2S/PS)
--    the payload length also depends on content - the cluster width field indicates length

-- header and payload data are packed into words of width "cL1DataFifoWidth", and written to the data FIFO
-- when the packet ends, metadata is stored in two separate synchronised FWFT FIFOs:
--    event FIFO - the local event number (for synchronisation at the DTC/DAQ level),
--    record FIFO - a record of number of words written to the data FIFO for a given event

-- (at DTC or DAQ path/SLINK level, not implemented here)
-- DAQ firmware should check the FWFT L1A event number in every event FIFO against TCDS record
-- if ready to readout, number of words to read from RAM can be determined from record FIFO

-- also not yet implemented:
--   timeouts/error states when we are stuck in any non-idle state for too long
--   monitoring for CIC health using idle pattern? overall FE health to go in a separate block probably
--   different modes e.g. un/sparsified decoding for 2S modules, PS module decoding
--   how to handle 10G PS data (assuming two input bits, instead of one)

----

begin


  event_number <= std_logic_vector(event_counter-1);


  decoder_sm: process (clk)
  
    variable status     : std_logic_vector(cL1StatusWidth - 1 downto 0);
    variable l1a        : std_logic_vector(cL1IdWidth - 1 downto 0);
    variable nPclusters : std_logic_vector(cL1NPcluster_width - 1 downto 0);
    variable nSclusters : std_logic_vector(CL1NScluster_width - 1 downto 0);
 
    variable word_count : unsigned(cL1CtrlFifoWidth - 1 downto 0) := ( others => '0' );

  begin
    if rising_edge(clk) then
    
      if reset = '1' then
      
        decoder        <= idle;
        start_counter  <= cL1StartSequenceThresh;
        event_counter  <= to_unsigned(1, cL1CtrlFifoWidth);
        
        event_fifo     <= ( others => '0' );
        record_fifo    <= ( others => '0' );
        data_fifo      <= ( others => '0' );
        
        data_fifo_wr   <= '0';
        event_fifo_wr  <= '0';
        record_fifo_wr <= '0';
        
      else
    
        data_fifo_wr   <= '0';
        event_fifo_wr  <= '0';
        record_fifo_wr <= '0';

        data <= data_in(0);          

        case decoder is
        
          -- wait for a start sequence of '1's indicating the start of a packet
          when idle =>
            -- idle pattern is a repeating 10101..
            if data = '1' then
              start_counter <= start_counter - 1;
            else
              start_counter <= cL1StartSequenceThresh;
            end if;
            if start_counter = 0 then
              -- if more than "cL1StartSequenceThresh" consecutive '1's, arm capture
              decoder <= capture_armed;
              start_counter <= cL1StartSequenceThresh;
            end if;

          -- wait for a single '0' indicating the start of header
          when capture_armed =>
            if data = '0' then
              decoder <= header_capture;
              header_counter <= cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth));
            end if;

          -- extract & store the header and calculate the payload length
          when header_capture =>
            -- shift in header
            header(0) <= data;
            header(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 1) <= header(cL1HeaderWidth(selectIndexFromModuleType(module_type, bandwidth)) - 2 downto 0);
         
            -- wait until full length of header has been shifted in
            if header_counter = 0 then
              -- start decoding payload on next clock cycle
              decoder <= payload_capture;
              -- PS sparsified mode

              status := header(cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth)) + cL1StatusWidth - 1 downto cL1StatusLow(selectIndexFromModuleType(module_type, bandwidth)));
              l1a := header(cL1IdLow(selectIndexFromModuleType(module_type, bandwidth)) + cL1IdWidth - 1 downto cL1IdLow(selectIndexFromModuleType(module_type, bandwidth)));

              -- nPclusters defines length of payload Pixels' data length
              nPclusters := header(cL1NPcluster_low + cL1NPcluster_width - 1 downto cL1NPcluster_low);

              -- nSclusters defines length of payload strips' data length
              nSclusters := header(cL1NScluster_low + cL1NScluster_width - 1 downto cL1NScluster_low);

              -- initialise counters for the payload_capture state
              -- DSP calculation? maybe too hard to calculate
              payload_counter <= to_integer(unsigned(nSclusters))*cL1SClusterWidth + to_integer(unsigned(nPclusters))*cL1PClusterWidth - 1;

              bit_counter <= cL1DataFifoWidth - 1;
              payload_word(0) <= data;

              -- store the header to the data FIFO, and set word count to 1 
              data_fifo <= l1a & status & nSclusters & nPclusters;
              data_fifo_wr <= '1';
              word_count := to_unsigned(1, cL1CtrlFifoWidth);
            else
              header_counter <= header_counter - 1;
            end if;
              
          -- pack the payload into words until the predetermined packet length ends
          when payload_capture =>
            -- shift in payload
            payload_word(0) <= data;
            payload_word(cL1DataFifoWidth - 1 downto 1) <= payload_word(cL1DataFifoWidth - 2 downto 0);

            -- wait until full length of payload has been shifted in to leave state
            if payload_counter <= 0 then
              decoder <= store_event;
            else
              payload_counter <= payload_counter - 1;
            end if;

            -- check to see if we have packed enough data to write to data FIFO
            if bit_counter = 0 then
         
              -- store the word to data FIFO, and increment word count
              data_fifo <= payload_word;

              data_fifo_wr <= '1';
              word_count := word_count + 1;
              bit_counter <= cL1DataFifoWidth - 1;
            else
              bit_counter <= bit_counter - 1;

              -- check if we have reached the end of the payload, but haven't fully packed a word
              if payload_counter = 0 then
              
                -- pad out payload word and store it to data FIFO
                data_fifo <= std_logic_vector(unsigned(payload_word) sll bit_counter);
                data_fifo_wr <= '1';
                word_count := word_count + 1;
              end if;
           
            end if;

          -- finish off packet handling by storing metadata to FIFOs, increment event counter, and reinitialise
          when store_event =>
            event_fifo <= std_logic_vector(event_counter);
            record_fifo <= std_logic_vector(word_count);
            event_fifo_wr <= '1';
            record_fifo_wr <= '1';
            
            decoder <= idle;
            word_count := (others => '0');
            event_counter <= event_counter + 1;
          
          when others =>
            decoder <= idle;
                                                    
        end case;

      end if;
    end if;
  end process;
  
  
  end modulePS5G;
