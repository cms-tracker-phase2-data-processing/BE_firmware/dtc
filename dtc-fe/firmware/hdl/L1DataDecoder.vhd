library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.module_constants.all;
use work.dtc_constants.all;

entity L1DataDecoder is
generic (
     module_type : string;
     bandwidth   : integer
    );

port (
     --- Input Ports ---
     clk                 : in std_logic;
     -- Assumed data_in(0) is the latest bit arrived, so data_in is significant from left to right
     data_in             : in std_logic_vector(cNumberOfL1ELinks(selectIndexFromModuleType(module_type, bandwidth)) - 1 downto 0);
     reset               : in std_logic;
     --- Output Ports ---
     event_number        : out std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);
     data_fifo           : out std_logic_vector(cL1DataFifoWidth - 1 downto 0);
     event_fifo          : out std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);
     record_fifo         : out std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);
     data_fifo_wr        : out std_logic;
     event_fifo_wr       : out std_logic;
     record_fifo_wr      : out std_logic
);
end L1DataDecoder;
