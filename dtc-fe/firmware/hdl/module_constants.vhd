library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package module_constants is

    type tModuleConstantIntegerArray is array (0 to 2) of integer;
    type tModuleConstantBooleanArray is array (0 to 2) of boolean;
    type tModuleConstantVectorArray is array (0 to 2) of std_logic_vector(6 downto 0);

    type tUnboundedIntegerArray is array(integer range <>) of integer;
    subtype tLinkIndicesArray is tUnboundedIntegerArray(0 to 12 - 1);
    subtype tL1AIndicesArray is tUnboundedIntegerArray(0 to 2 - 1);
    type tUnbounded2DIntegerArray is array(integer range <>) of tLinkIndicesArray;
    subtype tMultiCICLinkIndicesArray is tUnbounded2DIntegerArray(0 to 2 - 1);
    type tModuleConstantLinkIndicesArray is array (0 to 2) of tMultiCICLinkIndicesArray;
    type tUnboundedL1A2DIntegerArray is array(integer range <>) of tL1AIndicesArray;
    subtype tMultiCICL1AIndicesArray is tUnboundedL1A2DIntegerArray(0 to 2 - 1);
    type tModuleConstantL1AIndicesArray is array (0 to 2) of tMultiCICL1AIndicesArray;


    constant cGBTFrameWidth           : integer                     := 112;
    constant cNumberOfELinks          : tModuleConstantIntegerArray := (5, 6, 12);
    constant cNumberOfL1ELinks        : tModuleConstantIntegerArray := (1, 1, 2);

    constant cWordWidth               : tModuleConstantIntegerArray := (8, 8, 16);
    constant cStubWidth               : tModuleConstantIntegerArray := (18, 21, 21);
    constant cHeaderWidth             : integer                     := 28;
    constant cMaxStubs                : tModuleConstantIntegerArray := (16, 16, 35);
    constant cBoxCarFrames            : integer                     := 64;
    constant cHeaderFrames            : tModuleConstantIntegerArray := (6, 5, 3);
    constant cFirstStubOffset         : tModuleConstantIntegerArray := (3, 4, 4);

    constant cDataPeriod              : tModuleConstantIntegerArray := (8, 8, 8);

    constant cModuleBendWidth         : tModuleConstantIntegerArray := (4, 3, 3);
    constant cModuleRowWidth          : tModuleConstantIntegerArray := (11, 11, 11);
    constant cModuleStubBxWidth       : tModuleConstantIntegerArray := (3, 3, 3);
    constant cHeaderMultiplicityWidth : tModuleConstantIntegerArray := (6, 6, 6);
    constant cHeaderBxWidth           : tModuleConstantIntegerArray := (12, 12, 12);

    constant cAlignmentDepth          : integer                     := 8; -- Can be reduced if no realignment is necessary, reducing both latency and resource usage for the FrameAligner
    
    constant cL1StartSequenceThresh   : integer                     := 20;
    constant cL1HeaderWidth           : tModuleConstantIntegerArray := (18, 33, 33);
    constant cL1PClusterWidth         : integer                     := 17;
    constant cL1SClusterWidth         : integer                     := 14;
    constant cL1ClusterWidth          : integer                     := 14;

    constant cL1StatusWidth           : integer                     := 9;
    constant cL1StatusLow             : tModuleConstantIntegerArray := (9, 24, 24);
    constant cL1IdWidth               : integer                     := 9;
    constant cL1IdLow                 : tModuleConstantIntegerArray := (0, 15, 15);
    constant cL1NPcluster_width       : integer                     := 7;
    constant cL1NPcluster_low         : integer                     := 0;
    constant cL1NScluster_width       : integer                     := 7;
    constant cL1NScluster_low         : integer                     := 8;
    constant cL1NClusterWidth         : integer                     := 7;
    constant cL1NClusterLow           : integer                     := 1;

 
    constant cSparsifiedMode          : boolean                     := false;
    constant cL1UnsparsifiedLength    : integer                     := 2200;

    constant cHeaderSRDelay           : tModuleConstantVectorArray  := ("1000000", "0100000", "0001000"); -- CHECK THIS
    constant cHeaderSignalDelay       : tModuleConstantIntegerArray := (55, 56, 58); -- CHECK THIS

    constant link_indices     : tModuleConstantLinkIndicesArray := (
        (( 64,  72,  80,  88,  96,  -1,  -1,  -1,  -1,  -1,  -1,  -1), ( 8, 16, 24, 32, 40, -1, -1, -1, -1, -1, -1, -1)),
        ((  0, 104,  96,  88,  80,  64,  -1,  -1,  -1,  -1,  -1,  -1), (56, 48, 40, 32, 24,  8, -1, -1, -1, -1, -1, -1)),
        (( 0, 208, 192, 176, 160, 128,  0, 208, 192, 176, 160, 128), (112, 96, 80, 64, 48, 16, 112, 96, 80, 64, 48, 16))
    );
    constant l1a_indices      : tModuleConstantL1AIndicesArray  := (
        ((0,  -1), (56, -1)),
        ((72, -1), (16, -1)),
        ((144, 144),(32, 32))
    );

    function selectIndexFromModuleType(module_type : in string; bandwidth : in integer) return integer;

end package module_constants;

package body module_constants is

    function selectIndexFromModuleType(module_type : in string; bandwidth : in integer) return integer is
        variable index : integer;
    begin
        if module_type = "2S" then
            index := 0;
        elsif module_type = "PS" then
            if bandwidth = 5 then
                index := 1;
            elsif bandwidth = 10 then
                index := 2;
            else
                index := -1;
            end if;
        else
            index := -1;
        end if;
        return index;
    end selectIndexFromModuleType;

end package body module_constants;
