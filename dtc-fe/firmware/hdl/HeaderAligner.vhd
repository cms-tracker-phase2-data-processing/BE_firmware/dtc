----
-- Author: David Monk
-- Description file for HeaderAligner entity
-- This entity reads in the same stream as the StubExtractor and locates the
-- start of the boxcar. This information is then sent to the StubExtractor entity
-- to extract the stubs.
----


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.module_constants.all;


entity HeaderAligner is
    generic (
        module_type : string;
        bandwidth   : integer
    );
    port (
        --- Input Ports ---
        clk     : in std_logic;
        data_in : in lword     := LWORD_NULL;
        reset   : in std_logic := '0';
        --- Output Ports ---
        header_start : out std_logic;
        state        : out std_logic_vector(3 downto 0);
        sync_loss    : out std_logic
    );
end HeaderAligner;


architecture Behavorial of HeaderAligner is

    signal data_reg              : lword                        := LWORD_NULL;

    signal header_found          : std_logic                    := '0';
    signal header_start_internal : std_logic                    := '0';
    signal sync_loss_internal    : std_logic                    := '0';    
    signal counter               : unsigned(5 downto 0)         := (others => '0');

    type aligner_state is (unlocked, locking, locked);
    signal aligner : aligner_state := unlocked;
    
    signal align_state : std_logic_vector(2 downto 0) := (others => '0');
    signal sync_state  : std_logic                    := '0';

    signal sr_header_start : std_logic_vector(cHeaderSRDelay(selectIndexFromModuleType(module_type, bandwidth))'length - 1 downto 0) := (others => '0');
    

begin

    ff: process(clk)
    begin
        if rising_edge(clk) then
            data_reg <= data_in;
        end if;
    end process;


    header_start <= header_start_internal;
    state <= sync_state & align_state;
     

    align : process(clk)
    begin
        if rising_edge(clk) then
            if data_reg.strobe = '1' then

                if reset = '1' then
                    aligner <= unlocked;
                    sync_state <= '0';
                end if;

                case aligner is

                when unlocked =>
                    align_state <= "001";
                    header_start_internal <= '1';
                    sync_state <= '0';

                    if header_found = '1' then
                        counter <= to_unsigned(cHeaderSignalDelay(selectIndexFromModuleType(module_type, bandwidth)), 6);
                        aligner <= locking;
                    end if;

                when locking =>
                    align_state <= "010";
                    header_start_internal <= '1';
                    counter <= counter - 1;
                    sync_state <= '0';
                    
                    if counter = 0 then
                        aligner <= locked;
                        sync_state <= '1';
                    end if;

                when locked =>
                    align_state <= "100";
                    counter <= counter - 1;

                    if counter = 0 then
                        header_start_internal <= '1';
                    else
                        header_start_internal <= '0';
                    end if;
                    
                    if sync_loss_internal = '1' then
                      sync_state <= '0';
                    end if;                    

                when others =>
                    header_start_internal <= '1';
                    sync_state <= '0';

                end case;
            else
                case aligner is
                    when locked =>
                        header_start_internal <= '0';
                when others =>
                        header_start_internal <= '1';
                end case;
            end if;
        end if;
    end process;


    --==============================--
    -- Check data for header fingerprint
    --==============================--
    
    g2S : if module_type = "2S" generate
        --==============================--
        FingerPrintChecker: entity work.HeaderFingerprintChecker(module2S)
        --==============================--
        generic map (
            module_type => module_type,
            bandwidth   => bandwidth
        )
        port map (
            --- Input Ports ---
            clk => clk,
            data_in => data_reg,
            --- Output Ports ---
            header_found => header_found
        );
    end generate;
    gPS : if module_type = "PS" generate
        g5G : if bandwidth = 5 generate
            --==============================--
            FingerPrintChecker: entity work.HeaderFingerprintChecker(modulePS5G)
            --==============================--
            generic map (
                module_type => module_type,
                bandwidth   => bandwidth
            )
            port map (
                --- Input Ports ---
                clk => clk,
                data_in => data_reg,
                --- Output Ports ---
                header_found => header_found
            );
        end generate;
        g10G : if bandwidth = 10 generate
            --==============================--
            FingerPrintChecker: entity work.HeaderFingerprintChecker(modulePS10G)
            --==============================--
            generic map (
                module_type => module_type,
                bandwidth   => bandwidth
            )
            port map (
                --- Input Ports ---
                clk => clk,
                data_in => data_reg,
                --- Output Ports ---
                header_found => header_found
            );
        end generate;
    end generate;


    --==============================--
    -- Debugging
    --==============================--

    sync_loss <= sync_loss_internal;


    pSyncCheck : process(clk)
    begin
        if rising_edge(clk) and data_in.strobe = '1' then

            sr_header_start <= sr_header_start(sr_header_start'high - 1 downto sr_header_start'low) & header_start_internal;
            
            if aligner = locked then

                if sr_header_start = cHeaderSRDelay(selectIndexFromModuleType(module_type, bandwidth)) then
                    sync_loss_internal <= not header_found;
                else
                    sync_loss_internal <= '0';
                end if;

            else
                sync_loss_internal <= '0';
            end if;
    
        end if;
    end process;    

end architecture;
