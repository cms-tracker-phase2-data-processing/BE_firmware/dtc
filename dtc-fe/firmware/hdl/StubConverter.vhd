library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--use work.config.all;
use work.emp_data_types.all;

use work.module_constants.all;
use work.front_end_data_types.all;


entity StubConverter is
generic (
    module_type : string;
    bandwidth   : integer
);
port (
    --- Input Ports ---
    clk         : in  std_logic;
    stub_in     : in  lword := LWORD_NULL;
    --- Output Ports ---
    stub_out    : out lword := LWORD_NULL
);
end StubConverter;


architecture Behavorial of StubConverter is

begin

    stub_out.valid <= stub_in.valid;
    stub_out.strobe <= stub_in.strobe;
    stub_out.data(63 downto 22) <= stub_in.data(63 downto 22);

    gen2SCoversion: if module_type = "2S" generate
        stub_out.data(3 downto 0) <= (others => '0'); -- Z: no Z field in 2S data format
        stub_out.data(7 downto 4) <= stub_in.data(3 downto 0); -- Bend
        stub_out.data(15 downto 8) <= stub_in.data(11 downto 4); -- Address
        stub_out.data(18 downto 16) <= stub_in.data(14 downto 12); -- CBC ID
        stub_out.data(21 downto 19) <= stub_in.data(17 downto 15); -- Bx offset
    end generate gen2SCoversion;

    genPSCoversion: if module_type = "PS" generate
        stub_out.data(3 downto 0) <= stub_in.data(3 downto 0); -- Z
        stub_out.data(7 downto 4) <= '0' & stub_in.data(6 downto 4); -- Bend: PS bend field has only 3 bits, therefore a zero is prepended
        stub_out.data(15 downto 8) <= stub_in.data(14 downto 7); -- Address
        stub_out.data(18 downto 16) <= stub_in.data(17 downto 15); -- CBC ID
        stub_out.data(21 downto 19) <= stub_in.data(20 downto 18); -- Bx Offset
    end generate genPSCoversion;

end architecture;
