-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;

library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;
use work.emp_slink_types.all;


-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00002";

  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer               := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 36;
  constant CLOCK_RATIO        : integer               := 9;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (18, 9, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz

  -- Only used by nullalgo
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
    0      => kDummyRegion,                      -- PCIe, AXI & TCDS
    1      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    2      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    3      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    4      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    5      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    6      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    7      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    8      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    9      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    10     => (csp25, buf, no_fmt, buf, csp25),  -- ff-04
    11     => (csp25, buf, no_fmt, buf, csp25),  -- ff-04
    12     => (csp25, buf, no_fmt, buf, csp25),  -- ff-04
    13     => (csp25, buf, no_fmt, buf, csp25),  -- ff-05
    14     => (csp25, buf, no_fmt, buf, csp25),  -- ff-05
    15     => (csp25, buf, no_fmt, buf, csp25),  -- ff-05
    -- Cross-chip
    16     => (csp25, buf, no_fmt, buf, csp25),  -- ff-06
    17     => (csp25, buf, no_fmt, buf, csp25),  -- ff-06
    18     => (csp25, buf, no_fmt, buf, csp25),  -- ff-06
    19     => (csp25, buf, no_fmt, buf, csp25),  -- ff-07
    20     => (csp25, buf, no_fmt, buf, csp25),  -- ff-07
    21     => (csp25, buf, no_fmt, buf, csp25),  -- ff-07
    22     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    23     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    24     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    25     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    26     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    27     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    28     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    29     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    30     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    31     => (csp25, buf, no_fmt, buf, csp25),  -- ff-bi
    others => kDummyRegion
  );

  -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
  constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    1  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    2  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    3  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    4  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    5  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    6  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    7  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    8  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    9  => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    22 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    23 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    24 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    25 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    26 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    27 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    28 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    29 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    30 => ( 0=>(false, true, 0, false, lpgbtv0), 1=>(false, true, 0, false, lpgbtv0), 2=>(false, true, 0, false, lpgbtv0), 3=>(false, true, 0, false, lpgbtv0)),    
    others => kDummyRegionDataFramer
  );

  -- for lpgbt
  constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := (
    1  => (FEC12, DATARATE_5G12, PCS),
    2  => (FEC12, DATARATE_5G12, PCS),    
    3  => (FEC12, DATARATE_5G12, PCS),
    4  => (FEC12, DATARATE_5G12, PCS),
    5  => (FEC12, DATARATE_5G12, PCS),
    6  => (FEC12, DATARATE_5G12, PCS),    
    7  => (FEC12, DATARATE_5G12, PCS),
    8  => (FEC12, DATARATE_5G12, PCS),
    9  => (FEC12, DATARATE_5G12, PCS),
    22 => (FEC12, DATARATE_5G12, PCS),    
    23 => (FEC12, DATARATE_5G12, PCS),
    24 => (FEC12, DATARATE_5G12, PCS),
    25 => (FEC12, DATARATE_5G12, PCS),
    26 => (FEC12, DATARATE_5G12, PCS),    
    27 => (FEC12, DATARATE_5G12, PCS),
    28 => (FEC12, DATARATE_5G12, PCS),
    29 => (FEC12, DATARATE_5G12, PCS),
    30 => (FEC12, DATARATE_5G12, PCS),
    others => kDummyRegionLpgbt
  );

  -- Specify the slink quad using the corresponding region conf ID
  -- Specify slink channels to enable using the channel mask
  constant SLINK_CONF : slink_conf_array_t := (
    others => kNoSlink
  ); 


end emp_project_decl;
-------------------------------------------------------------------------------
