library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is
    constant cNumberOfFEModules   : integer := 72;
    constant cNumberOfOutputLinks : integer := 36;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap : tDTCInputLinkMap := (
        0  => (  4,  "2S", 5, "CIC2"),
        1  => (  5,  "2S", 5, "CIC2"),
        2  => (  6,  "2S", 5, "CIC2"),
        3  => (  7,  "2S", 5, "CIC2"),
        4  => (  8,  "2S", 5, "CIC2"),
        5  => (  9,  "2S", 5, "CIC2"),
        6  => ( 10,  "2S", 5, "CIC2"),
        7  => ( 11,  "2S", 5, "CIC2"),
        8  => ( 12,  "2S", 5, "CIC2"),
        9  => ( 13,  "2S", 5, "CIC2"),
        10 => ( 14,  "2S", 5, "CIC2"),
        11 => ( 15,  "2S", 5, "CIC2"),
        12 => ( 16,  "2S", 5, "CIC2"),
        13 => ( 17,  "2S", 5, "CIC2"),
        14 => ( 18,  "2S", 5, "CIC2"),
        15 => ( 19,  "2S", 5, "CIC2"),
        16 => ( 20,  "2S", 5, "CIC2"),
        17 => ( 21,  "2S", 5, "CIC2"),
        18 => ( 22,  "2S", 5, "CIC2"),
        19 => ( 23,  "2S", 5, "CIC2"),
        20 => ( 24,  "2S", 5, "CIC2"),
        21 => ( 25,  "2S", 5, "CIC2"),
        22 => ( 26,  "2S", 5, "CIC2"),
        23 => ( 27,  "2S", 5, "CIC2"),
        24 => ( 28,  "2S", 5, "CIC2"),
        25 => ( 29,  "2S", 5, "CIC2"),
        26 => ( 30,  "2S", 5, "CIC2"),
        27 => ( 31,  "2S", 5, "CIC2"),
        28 => ( 32,  "2S", 5, "CIC2"),
        29 => ( 33,  "2S", 5, "CIC2"),
        30 => ( 34,  "2S", 5, "CIC2"),
        31 => ( 35,  "2S", 5, "CIC2"),
        32 => ( 36,  "2S", 5, "CIC2"),
        33 => ( 37,  "2S", 5, "CIC2"),
        34 => ( 38,  "2S", 5, "CIC2"),
        35 => ( 39,  "2S", 5, "CIC2"),
        36 => ( 88,  "2S", 5, "CIC2"),
        37 => ( 89,  "2S", 5, "CIC2"),
        38 => ( 90,  "2S", 5, "CIC2"),
        39 => ( 91,  "2S", 5, "CIC2"),
        40 => ( 92,  "2S", 5, "CIC2"),
        41 => ( 93,  "2S", 5, "CIC2"),
        42 => ( 94,  "2S", 5, "CIC2"),
        43 => ( 95,  "2S", 5, "CIC2"),
        44 => ( 96,  "2S", 5, "CIC2"),
        45 => ( 97,  "2S", 5, "CIC2"),
        46 => ( 98,  "2S", 5, "CIC2"),
        47 => ( 99,  "2S", 5, "CIC2"),
        48 => (100,  "2S", 5, "CIC2"),
        49 => (101,  "2S", 5, "CIC2"),
        50 => (102,  "2S", 5, "CIC2"),
        51 => (103,  "2S", 5, "CIC2"),
        52 => (104,  "2S", 5, "CIC2"),
        53 => (105,  "2S", 5, "CIC2"),
        54 => (106,  "2S", 5, "CIC2"),
        55 => (107,  "2S", 5, "CIC2"),
        56 => (108,  "2S", 5, "CIC2"),
        57 => (109,  "2S", 5, "CIC2"),
        58 => (110,  "2S", 5, "CIC2"),
        59 => (111,  "2S", 5, "CIC2"),
        60 => (112,  "2S", 5, "CIC2"),
        61 => (113,  "2S", 5, "CIC2"),
        62 => (114,  "2S", 5, "CIC2"),
        63 => (115,  "2S", 5, "CIC2"),
        64 => (116,  "2S", 5, "CIC2"),
        65 => (117,  "2S", 5, "CIC2"),
        66 => (118,  "2S", 5, "CIC2"),
        67 => (119,  "2S", 5, "CIC2"),
        68 => (120,  "2S", 5, "CIC2"),
        69 => (121,  "2S", 5, "CIC2"),
        70 => (122,  "2S", 5, "CIC2"),
        71 => (123,  "2S", 5, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap : tDTCOutputLinkMap := (
46,  47,  48,  49,  50,  51,  52,  53,  54,
55,  56,  57,  58,  59,  60,  61,  62,  63,
64,  65,  66,  67,  68,  69,  70,  71,  72,
73,  74,  75,  76,  77,  78,  79,  80,  81
    );

end package dtc_link_maps;
