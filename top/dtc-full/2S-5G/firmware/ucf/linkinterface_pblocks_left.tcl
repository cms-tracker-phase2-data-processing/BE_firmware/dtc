create_pblock pblock_p_quad_22
resize_pblock pblock_p_quad_22 -add CLOCKREGION_X1Y9:CLOCKREGION_X2Y9
add_cells_to_pblock [get_pblocks pblock_p_quad_22] [get_cells -quiet [list {payload/module[36].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_22] [get_cells -quiet [list {payload/module[37].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_22] [get_cells -quiet [list {payload/module[38].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_22] [get_cells -quiet [list {payload/module[39].LinkInterface}]]

create_pblock pblock_p_quad_23
resize_pblock pblock_p_quad_23 -add CLOCKREGION_X1Y8:CLOCKREGION_X2Y8
add_cells_to_pblock [get_pblocks pblock_p_quad_23] [get_cells -quiet [list {payload/module[40].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_23] [get_cells -quiet [list {payload/module[41].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_23] [get_cells -quiet [list {payload/module[42].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_23] [get_cells -quiet [list {payload/module[43].LinkInterface}]]

create_pblock pblock_p_quad_24
resize_pblock pblock_p_quad_24 -add CLOCKREGION_X1Y7:CLOCKREGION_X2Y7
add_cells_to_pblock [get_pblocks pblock_p_quad_24] [get_cells -quiet [list {payload/module[44].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_24] [get_cells -quiet [list {payload/module[45].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_24] [get_cells -quiet [list {payload/module[46].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_24] [get_cells -quiet [list {payload/module[47].LinkInterface}]]

create_pblock pblock_p_quad_25
resize_pblock pblock_p_quad_25 -add CLOCKREGION_X1Y6:CLOCKREGION_X2Y6
add_cells_to_pblock [get_pblocks pblock_p_quad_25] [get_cells -quiet [list {payload/module[48].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_25] [get_cells -quiet [list {payload/module[49].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_25] [get_cells -quiet [list {payload/module[50].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_25] [get_cells -quiet [list {payload/module[51].LinkInterface}]]

create_pblock pblock_p_quad_26
resize_pblock pblock_p_quad_26 -add CLOCKREGION_X1Y5:CLOCKREGION_X2Y5
add_cells_to_pblock [get_pblocks pblock_p_quad_26] [get_cells -quiet [list {payload/module[52].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_26] [get_cells -quiet [list {payload/module[53].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_26] [get_cells -quiet [list {payload/module[54].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_26] [get_cells -quiet [list {payload/module[55].LinkInterface}]]

create_pblock pblock_p_quad_27
resize_pblock pblock_p_quad_27 -add CLOCKREGION_X1Y4:CLOCKREGION_X2Y4
add_cells_to_pblock [get_pblocks pblock_p_quad_27] [get_cells -quiet [list {payload/module[56].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_27] [get_cells -quiet [list {payload/module[57].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_27] [get_cells -quiet [list {payload/module[58].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_27] [get_cells -quiet [list {payload/module[59].LinkInterface}]]

create_pblock pblock_p_quad_28
resize_pblock pblock_p_quad_28 -add CLOCKREGION_X1Y3:CLOCKREGION_X2Y3
add_cells_to_pblock [get_pblocks pblock_p_quad_28] [get_cells -quiet [list {payload/module[60].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_28] [get_cells -quiet [list {payload/module[61].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_28] [get_cells -quiet [list {payload/module[62].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_28] [get_cells -quiet [list {payload/module[63].LinkInterface}]]

create_pblock pblock_p_quad_29
resize_pblock pblock_p_quad_29 -add CLOCKREGION_X1Y2:CLOCKREGION_X2Y2
add_cells_to_pblock [get_pblocks pblock_p_quad_29] [get_cells -quiet [list {payload/module[64].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_29] [get_cells -quiet [list {payload/module[65].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_29] [get_cells -quiet [list {payload/module[66].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_29] [get_cells -quiet [list {payload/module[67].LinkInterface}]]

create_pblock pblock_p_quad_30
resize_pblock pblock_p_quad_30 -add CLOCKREGION_X1Y1:CLOCKREGION_X2Y1
add_cells_to_pblock [get_pblocks pblock_p_quad_30] [get_cells -quiet [list {payload/module[68].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_30] [get_cells -quiet [list {payload/module[69].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_30] [get_cells -quiet [list {payload/module[70].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_30] [get_cells -quiet [list {payload/module[71].LinkInterface}]]
