create_pblock pblock_p_quad_1
resize_pblock pblock_p_quad_1 -add CLOCKREGION_X5Y1:CLOCKREGION_X6Y1
add_cells_to_pblock [get_pblocks pblock_p_quad_1] [get_cells -quiet [list {payload/module[0].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_1] [get_cells -quiet [list {payload/module[1].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_1] [get_cells -quiet [list {payload/module[2].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_1] [get_cells -quiet [list {payload/module[3].LinkInterface}]]

create_pblock pblock_p_quad_2
resize_pblock pblock_p_quad_2 -add CLOCKREGION_X5Y2:CLOCKREGION_X6Y2
add_cells_to_pblock [get_pblocks pblock_p_quad_2] [get_cells -quiet [list {payload/module[4].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_2] [get_cells -quiet [list {payload/module[5].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_2] [get_cells -quiet [list {payload/module[6].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_2] [get_cells -quiet [list {payload/module[7].LinkInterface}]]

create_pblock pblock_p_quad_3
resize_pblock pblock_p_quad_3 -add CLOCKREGION_X5Y3:CLOCKREGION_X6Y3
add_cells_to_pblock [get_pblocks pblock_p_quad_3] [get_cells -quiet [list {payload/module[8].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_3] [get_cells -quiet [list {payload/module[9].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_3] [get_cells -quiet [list {payload/module[10].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_3] [get_cells -quiet [list {payload/module[11].LinkInterface}]]

create_pblock pblock_p_quad_4
resize_pblock pblock_p_quad_4 -add CLOCKREGION_X5Y4:CLOCKREGION_X6Y4
add_cells_to_pblock [get_pblocks pblock_p_quad_4] [get_cells -quiet [list {payload/module[12].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_4] [get_cells -quiet [list {payload/module[13].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_4] [get_cells -quiet [list {payload/module[14].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_4] [get_cells -quiet [list {payload/module[15].LinkInterface}]]

create_pblock pblock_p_quad_5
resize_pblock pblock_p_quad_5 -add CLOCKREGION_X5Y5:CLOCKREGION_X6Y5
add_cells_to_pblock [get_pblocks pblock_p_quad_5] [get_cells -quiet [list {payload/module[16].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_5] [get_cells -quiet [list {payload/module[17].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_5] [get_cells -quiet [list {payload/module[18].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_5] [get_cells -quiet [list {payload/module[19].LinkInterface}]]

create_pblock pblock_p_quad_6
resize_pblock pblock_p_quad_6 -add CLOCKREGION_X5Y6:CLOCKREGION_X6Y6
add_cells_to_pblock [get_pblocks pblock_p_quad_6] [get_cells -quiet [list {payload/module[20].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_6] [get_cells -quiet [list {payload/module[21].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_6] [get_cells -quiet [list {payload/module[22].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_6] [get_cells -quiet [list {payload/module[23].LinkInterface}]]

create_pblock pblock_p_quad_7
resize_pblock pblock_p_quad_7 -add CLOCKREGION_X5Y7:CLOCKREGION_X6Y7
add_cells_to_pblock [get_pblocks pblock_p_quad_7] [get_cells -quiet [list {payload/module[24].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_7] [get_cells -quiet [list {payload/module[25].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_7] [get_cells -quiet [list {payload/module[26].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_7] [get_cells -quiet [list {payload/module[27].LinkInterface}]]

create_pblock pblock_p_quad_8
resize_pblock pblock_p_quad_8 -add CLOCKREGION_X5Y8:CLOCKREGION_X6Y8
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[28].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[29].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[30].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[31].LinkInterface}]]

create_pblock pblock_p_quad_9
resize_pblock pblock_p_quad_9 -add CLOCKREGION_X5Y9:CLOCKREGION_X6Y9
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[32].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[33].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[34].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[35].LinkInterface}]]
