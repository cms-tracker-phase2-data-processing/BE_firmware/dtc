library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  ---
  use work.ipbus.all;
  use work.ipbus_reg_types.all;
  use work.ipbus_decode_emp_payload.all;
  ---
  use work.emp_data_types.all;
  use work.emp_project_decl.all;
  use work.emp_device_decl.all;
  use work.emp_ttc_decl.all;
  use work.emp_slink_types.all;
  ---
  use work.dtc_link_maps.all;
  use work.dtc_constants.all;
  use work.dtc_data_types.all;


entity emp_payload is
  port (
    --- Input Ports ---
    clk_p          : in    std_logic;
    clk40          : in    std_logic := '0';
    clk_payload    : in    std_logic_vector(2 downto 0);
    rst_payload    : in    std_logic_vector(2 downto 0);
    rst_loc        : in    std_logic_vector(N_REGION - 1 downto 0);
    clken_loc      : in    std_logic_vector(N_REGION - 1 downto 0);
    ctrs           : in    ttc_stuff_array;
    d              : in    ldata(4 * N_REGION - 1 downto 0);
    backpressure   : in    std_logic_vector(SLINK_MAX_QUADS-1 downto 0);
    --- Output Ports ---
    bc0            : out   std_logic;
    gpio           : out   std_logic_vector(29 downto 0);
    gpio_en        : out   std_logic_vector(29 downto 0);
    q              : out   ldata(4 * N_REGION - 1 downto 0);
    slink_q        : out   slink_input_data_quad_array(SLINK_MAX_QUADS-1 downto 0);
    --- IPBus Ports ---
    clk            : in    std_logic;
    rst            : in    std_logic;
    ipb_in         : in    ipb_wbus;
    ipb_out        : out   ipb_rbus
  );
end entity emp_payload;


architecture rtl of emp_payload is

  signal data_in                     : ldata(4 * N_REGION - 1 downto 0);
  signal data_out                    : ldata(4 * N_REGION - 1 downto 0);

  -- IPBus fabric

  signal ipb_to_slaves              : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves            : ipb_rbus_array(N_SLAVES - 1 downto 0);

  signal fe_control_registers       : ipb_reg_v(0 downto 0);
  signal fe_status_registers        : ipb_reg_v(0 downto 0);


  -- FE data extraction and monitoring

  signal ipb_chain                  : ipbdc_bus_array(cNumberOfFEModules downto 0);
  signal stubs                      : ldata(cNumberOfFEModules - 1 downto 0);

  signal header_array                                    : tCICHeaderArray(cNumberOfFEModules*cNumberOfCICs - 1 downto 0)  := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
  signal header_start_array, header_start_array_buffered : std_logic_vector(cNumberOfFEModules*cNumberOfCICs - 1 downto 0) := (others => '0');


  -- Daqpath
    
  signal daq_read                   : tDaqFlagLinkArray(cNumberOfFEModules - 1 downto 0);
  signal daq_data                   : tDaqDataLinkArray(cNumberOfFEModules - 1 downto 0);
  signal daq_empty                  : tDaqFlagLinkArray(cNumberOfFEModules - 1 downto 0);

  -- BE Stub Processing and Routing

  signal router_output              : ldata(cNumberOfOutputLinks - 1 downto 0) := (others => LWORD_NULL);

  type lword_shift_register is array(5 downto 0) of ldata(cNumberOfFEModules - 1 downto 0);
  signal stubs_shift_register : lword_shift_register := (others => (others => LWORD_NULL));
  attribute SHREG_EXTRACT       : string;
  attribute SHREG_EXTRACT of stubs_shift_register : signal is "no";  -- Don't absorb FFs into shreg

  signal stub_processor_input       : ldata(cNumberOfFEModules - 1 downto 0) := (others => LWORD_NULL);


  -- TCDS & Fast commands

  signal global_fcmd : tFastCommand;
  signal tcds_fcmd   : tFastCommand;
  signal local_bc0   : std_logic;  


begin


  --==============================--
  -- IPBus fabric
  --==============================--

  --==============================--
  fabric : entity work.ipbus_fabric_sel
  --==============================--
  generic map (
    NSLV      => N_SLAVES,
    SEL_WIDTH => IPBUS_SEL_WIDTH
  )
  port map (
    ipb_in          => ipb_in,
    ipb_out         => ipb_out,
    sel             => ipbus_sel_emp_payload(ipb_in.ipb_addr),
    ipb_to_slaves   => ipb_to_slaves,
    ipb_from_slaves => ipb_from_slaves
  );

  --==============================--
  channel_ctrl : entity work.ipbus_ctrlreg_v
  --==============================--
  generic map (
    N_CTRL            => 1,
    N_STAT            => 1
  )
  port map (
    clk       => clk,
    reset     => rst,
    ipbus_in  => ipb_to_slaves(N_SLV_FE),
    ipbus_out => ipb_from_slaves(N_SLV_FE),
    d         => fe_status_registers,
    q		      => fe_control_registers
  );

  --==============================--
  channel_select : entity work.ipbus_dc_fabric_sel
  --==============================--
  generic map (
    SEL_WIDTH => 7
  )
  port map (
    clk       => clk,
    rst       => rst,
    sel       => fe_control_registers(0)(6 downto 0),
    ipb_in    => ipb_to_slaves(N_SLV_FE_CHAN),
    ipb_out   => ipb_from_slaves(N_SLV_FE_CHAN),
    ipbdc_out => ipb_chain(0),
    ipbdc_in  => ipb_chain(cNumberOfFEModules)
  );

  fe_status_registers(0) <= std_logic_vector(to_unsigned(cNumberOfFEModules, 32));


  --==============================--
  -- FE data extraction and monitoring
  --==============================--

  --==============================--
  module : for i in 0 to cNumberOfFEModules - 1 generate
    --==============================--

    signal ipb_to_channel   : ipb_wbus;
    signal ipb_from_channel : ipb_rbus;

  begin

    --==============================--
    channel_node : entity work.ipbus_dc_node
    --==============================--
    generic map (
      I_SLV     => i,
      SEL_WIDTH => 7,
      PIPELINE  => true
    )
    port map (
      clk       => clk,
      rst       => rst,
      ipb_out   => ipb_to_channel,
      ipb_in    => ipb_from_channel,
      ipbdc_in  => ipb_chain(i),
      ipbdc_out => ipb_chain(i + 1)
    );

    --==============================--
    LinkInterface : entity work.LinkInterface
    --==============================--
    generic map (
      module_type => cDTCInputLinkMap(i).module_type,
      bandwidth   => cDTCInputLinkMap(i).bandwidth,
      cic_type    => cDTCInputLinkMap(i).cic_type,
      enable_monitoring => true
    )
    port map (
      --- Input Ports ---
      clk_p              => clk_p,
      clk40              => clk40,
      link_in            => data_in(cDTCInputLinkMap(i).index),
      daq_read           => daq_read(i),
      global_fcmd        => global_fcmd,
      --- Output Ports ---
      link_out           => data_out(cDTCInputLinkMap(i).index),
      stub_out           => stubs(i),
      header_out         => header_array(cNumberOfCICs * i + cNumberOfCICs - 1 downto cNumberOfCICs * i),
      daq_out            => daq_data(i),
      daq_empty          => daq_empty(i),
      --- IPBus Ports ---
      clk                => clk,
      rst                => rst,
      ipb_in             => ipb_to_channel,
      ipb_out            => ipb_from_channel,
      --- Debug Ports ---
      debug_header_start => header_start_array(cNumberOfCICs*(i + 1) - 1 downto cNumberOfCICs*i)
    );

  end generate module;


  --==============================--
  -- Global TCDS
  --==============================--

  --==============================--
  GlobalFastCommand: entity work.GlobalFastCommand
  --==============================--
  port map(
    --- Input Ports ---
    clk_p            => clk_p,
    clk40            => clk40,
    ext_fcmd         => tcds_fcmd,
    --- Output Ports ---
    global_fcmd      => global_fcmd,
    --- IPBus Ports ---
    clk              => clk,
    rst              => rst,
    ipb_in           => ipb_to_slaves(N_SLV_TCDS_FAST_CMD),
    ipb_out          => ipb_from_slaves(N_SLV_TCDS_FAST_CMD)
  );
  
  -- temporary until provided by fwk
  --==============================--
  bc0_strobe: process(clk40)
  --==============================--

    variable timer : natural range 0 to 3563 := 3563;

  begin

    if rising_edge(clk40) then

        if timer = 0 then
            timer := 3563;
            local_bc0 <= '1';
        else
            timer := timer - 1;
            local_bc0 <= '0';
        end if;

    end if;
  end process bc0_strobe;

  tcds_fcmd.fast_reset    <= '0';
  tcds_fcmd.l1a_trig      <= '0';
  tcds_fcmd.cal_pulse     <= '0';
  tcds_fcmd.counter_reset <= local_bc0;

  
  --==============================--
  -- L1 Data Aggregator
  --==============================--

  -- --==============================--
  -- L1DataAggregator0 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(11 downto 0),
  --     empty   => daq_empty(11 downto 0),
  --     --- Output Ports ---
  --     read    => daq_read(11 downto 0),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_0),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_0)
  -- );
  
  -- --==============================--
  -- L1DataAggregator1 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(23 downto 12),
  --     empty   => daq_empty(23 downto 12),
  --     --- Output Ports ---
  --     read    => daq_read(23 downto 12),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_1),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_1)
  -- );

  -- --==============================--
  -- L1DataAggregator2 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(35 downto 24),
  --     empty   => daq_empty(35 downto 24),
  --     --- Output Ports ---
  --     read    => daq_read(35 downto 24),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_2),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_2)
  -- );

  -- --==============================--
  -- L1DataAggregator3 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(47 downto 36),
  --     empty   => daq_empty(47 downto 36),
  --     --- Output Ports ---
  --     read    => daq_read(47 downto 36),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_3),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_3)
  -- );

  -- --==============================--
  -- L1DataAggregator4 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(59 downto 48),
  --     empty   => daq_empty(59 downto 48),
  --     --- Output Ports ---
  --     read    => daq_read(59 downto 48),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_4),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_4)
  -- );

  -- --==============================--
  -- L1DataAggregator5 : entity work.L1DataAggregator
  -- --==============================--
  -- generic map (
  --   INPUT_LINKS => 12
  -- )
  -- port map (
  --     --- Input Ports ---
  --     clk_p   => clk_p,
  --     daq_in  => daq_data(71 downto 60),
  --     empty   => daq_empty(71 downto 60),
  --     --- Output Ports ---
  --     read    => daq_read(71 downto 60),
  --     --- IPBus Ports ---
  --     clk     => clk,
  --     rst     => rst,
  --     ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ_5),
  --     ipb_out => ipb_from_slaves(N_SLV_BE_DAQ_5)
  -- );

  --==============================--
  -- BE Stub Formatting and Routing
  --==============================--

  --==============================--
  StubProcessor : entity work.StubProcessor
  --==============================--
  port map (
    --- Input Ports ---
    clk_p    => clk_p,
    dtc_din  => stub_processor_input,
    --- Output Ports ---
    dtc_dout => router_output
  );

  --==============================--
  map_output : for i in 0 to cNumberOfOutputLinks - 1 generate
    --==============================--

    data_out(cDTCOutputLinkMap(i)) <= router_output(i);

  end generate map_output;


  bc0     <= '0';
  gpio    <= (others => '0');
  gpio_en <= (others => '0');


  slink_q_gen : for q in SLINK_MAX_QUADS-1 downto 0 generate
  begin
    slink_q(q) <= SLINK_INPUT_DATA_ARRAY_NULL;
  end generate slink_q_gen;


  --==============================--
  -- latch data inputs and outputs to payload
  --==============================--

  ff: process(clk_p)
  begin
    if rising_edge(clk_p) then
      data_in <= d;
      q <= data_out;

      stubs_shift_register(0) <= stubs;
      stubs_shift_register(5 downto 1) <= stubs_shift_register(5 - 1 downto 0);
      stub_processor_input <= stubs_shift_register(5);
    end if;
  end process;


end architecture rtl;
