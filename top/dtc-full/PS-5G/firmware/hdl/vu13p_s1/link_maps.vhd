library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is
    constant cNumberOfFEModules   : integer := 72;
    constant cNumberOfOutputLinks : integer := 36;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap : tDTCInputLinkMap := (
        0  => (  4,  "PS", 5, "CIC2"),
        1  => (  5,  "PS", 5, "CIC2"),
        2  => (  6,  "PS", 5, "CIC2"),
        3  => (  7,  "PS", 5, "CIC2"),
        4  => (  8,  "PS", 5, "CIC2"),
        5  => (  9,  "PS", 5, "CIC2"),
        6  => ( 10,  "PS", 5, "CIC2"),
        7  => ( 11,  "PS", 5, "CIC2"),
        8  => ( 12,  "PS", 5, "CIC2"),
        9  => ( 13,  "PS", 5, "CIC2"),
        10 => ( 14,  "PS", 5, "CIC2"),
        11 => ( 15,  "PS", 5, "CIC2"),
        12 => ( 16,  "PS", 5, "CIC2"),
        13 => ( 17,  "PS", 5, "CIC2"),
        14 => ( 18,  "PS", 5, "CIC2"),
        15 => ( 19,  "PS", 5, "CIC2"),
        16 => ( 20,  "PS", 5, "CIC2"),
        17 => ( 21,  "PS", 5, "CIC2"),
        18 => ( 22,  "PS", 5, "CIC2"),
        19 => ( 23,  "PS", 5, "CIC2"),
        20 => ( 24,  "PS", 5, "CIC2"),
        21 => ( 25,  "PS", 5, "CIC2"),
        22 => ( 26,  "PS", 5, "CIC2"),
        23 => ( 27,  "PS", 5, "CIC2"),
        24 => ( 28,  "PS", 5, "CIC2"),
        25 => ( 29,  "PS", 5, "CIC2"),
        26 => ( 30,  "PS", 5, "CIC2"),
        27 => ( 31,  "PS", 5, "CIC2"),
        28 => ( 32,  "PS", 5, "CIC2"),
        29 => ( 33,  "PS", 5, "CIC2"),
        30 => ( 34,  "PS", 5, "CIC2"),
        31 => ( 35,  "PS", 5, "CIC2"),
        32 => ( 36,  "PS", 5, "CIC2"),
        33 => ( 37,  "PS", 5, "CIC2"),
        34 => ( 38,  "PS", 5, "CIC2"),
        35 => ( 39,  "PS", 5, "CIC2"),
        36 => ( 88,  "PS", 5, "CIC2"),
        37 => ( 89,  "PS", 5, "CIC2"),
        38 => ( 90,  "PS", 5, "CIC2"),
        39 => ( 91,  "PS", 5, "CIC2"),
        40 => ( 92,  "PS", 5, "CIC2"),
        41 => ( 93,  "PS", 5, "CIC2"),
        42 => ( 94,  "PS", 5, "CIC2"),
        43 => ( 95,  "PS", 5, "CIC2"),
        44 => ( 96,  "PS", 5, "CIC2"),
        45 => ( 97,  "PS", 5, "CIC2"),
        46 => ( 98,  "PS", 5, "CIC2"),
        47 => ( 99,  "PS", 5, "CIC2"),
        48 => (100,  "PS", 5, "CIC2"),
        49 => (101,  "PS", 5, "CIC2"),
        50 => (102,  "PS", 5, "CIC2"),
        51 => (103,  "PS", 5, "CIC2"),
        52 => (104,  "PS", 5, "CIC2"),
        53 => (105,  "PS", 5, "CIC2"),
        54 => (106,  "PS", 5, "CIC2"),
        55 => (107,  "PS", 5, "CIC2"),
        56 => (108,  "PS", 5, "CIC2"),
        57 => (109,  "PS", 5, "CIC2"),
        58 => (110,  "PS", 5, "CIC2"),
        59 => (111,  "PS", 5, "CIC2"),
        60 => (112,  "PS", 5, "CIC2"),
        61 => (113,  "PS", 5, "CIC2"),
        62 => (114,  "PS", 5, "CIC2"),
        63 => (115,  "PS", 5, "CIC2"),
        64 => (116,  "PS", 5, "CIC2"),
        65 => (117,  "PS", 5, "CIC2"),
        66 => (118,  "PS", 5, "CIC2"),
        67 => (119,  "PS", 5, "CIC2"),
        68 => (120,  "PS", 5, "CIC2"),
        69 => (121,  "PS", 5, "CIC2"),
        70 => (122,  "PS", 5, "CIC2"),
        71 => (123,  "PS", 5, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap : tDTCOutputLinkMap := (
46,  47,  48,  49,  50,  51,  52,  53,  54,
55,  56,  57,  58,  59,  60,  61,  62,  63,
64,  65,  66,  67,  68,  69,  70,  71,  72,
73,  74,  75,  76,  77,  78,  79,  80,  81
    );

end package dtc_link_maps;
