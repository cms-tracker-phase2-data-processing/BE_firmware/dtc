library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    constant cNumberOfFEModules   : integer := 12;
    constant cNumberOfOutputLinks : integer := 12;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (
        0  => (32, "2S",  5, "CIC2"),
        1  => (33, "2S",  5, "CIC2"),
        2  => (34, "2S",  5, "CIC2"),
        3  => (35, "PS",  5, "CIC2"),
        4  => (36, "2S",  5, "CIC2"),
        5  => (37, "2S",  5, "CIC2"),
        6  => (38, "2S",  5, "CIC2"),
        7  => (39, "2S",  5, "CIC2"),
        8  => (40, "PS", 10, "CIC2"),
        9  => (41, "PS", 10, "CIC2"),
        10 => (42, "PS", 10, "CIC2"),
        11 => (43, "PS", 10, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (
        44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55
    );

end package dtc_link_maps;
