set_property USER_SLR_ASSIGNMENT quad_8 [get_cells {payload/module[0].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_8 [get_cells {payload/module[1].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_8 [get_cells {payload/module[2].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_8 [get_cells {payload/module[3].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_8 [get_cells {datapath/rgen[8].region}]

set_property USER_SLR_ASSIGNMENT quad_9 [get_cells {payload/module[4].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_9 [get_cells {payload/module[5].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_9 [get_cells {payload/module[6].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_9 [get_cells {payload/module[7].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_9 [get_cells {datapath/rgen[9].region}]

set_property USER_SLR_ASSIGNMENT quad_10 [get_cells {payload/module[8].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_10 [get_cells {payload/module[9].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_10 [get_cells {payload/module[10].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_10 [get_cells {payload/module[11].LinkInterface}]
set_property USER_SLR_ASSIGNMENT quad_10 [get_cells {datapath/rgen[10].region}]


create_pblock pblock_p_quad_8
resize_pblock pblock_p_quad_8 -add CLOCKREGION_X4Y9:CLOCKREGION_X6Y8
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[0].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[1].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[2].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[3].LinkInterface}]]

create_pblock pblock_p_quad_9
resize_pblock pblock_p_quad_9 -add CLOCKREGION_X4Y9:CLOCKREGION_X6Y9
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[4].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[5].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[6].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[7].LinkInterface}]]

create_pblock pblock_p_quad_10
resize_pblock pblock_p_quad_10 -add CLOCKREGION_X4Y10:CLOCKREGION_X6Y10
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[8].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[9].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[10].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[11].LinkInterface}]]
