library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    constant cNumberOfFEModules   : integer := 5;
    constant cNumberOfOutputLinks : integer := 5;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (
        0  => (4,  "PS", 5,  "CIC1"),
        1  => (5,  "PS", 10, "CIC2"),
        2  => (8,  "2S", 5,  "CIC2"),
        3  => (10, "2S", 5,  "CIC2"),
        4  => (11, "2S", 5,  "CIC1")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (
        16, 17, 18, 19, 20
    );

end package dtc_link_maps;
