create_pblock pblock_p_quad_8
resize_pblock pblock_p_quad_8 -add CLOCKREGION_X5Y8:CLOCKREGION_X6Y8
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[0].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[1].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[2].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[3].LinkInterface}]]
