library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    constant cNumberOfFEModules   : integer := 12;
    constant cNumberOfOutputLinks : integer := 4;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (
        0  => (32, "2S", 5, "CIC2"),
        1  => (33, "2S", 5, "CIC2"),
        2  => (34, "2S", 5, "CIC2"),
        3  => (35, "2S", 5, "CIC2"),
        4  => (36, "2S", 5, "CIC2"),
        5  => (37, "2S", 5, "CIC2"),
        6  => (38, "2S", 5, "CIC2"),
        7  => (39, "2S", 5, "CIC2"),
        8  => (40, "2S", 5, "CIC2"),
        9  => (41, "2S", 5, "CIC2"),
        10 => (42, "2S", 5, "CIC2"),
        11 => (43, "2S", 5, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (
        12, 13, 14, 15
    );

end package dtc_link_maps;
