-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;

library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;
use work.emp_slink_types.all;


-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00002";

  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer               := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 32;
  constant CLOCK_RATIO        : integer               := 8;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (16, 8, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz

  -- Only used by nullalgo
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
--    0      => (csp25, buf, no_fmt, buf, csp25),
--    1      => (csp25, buf, no_fmt, buf, csp25),
--    2      => (lpgbt, buf, no_fmt, buf, lpgbt),
    3      => (no_mgt, buf, no_fmt, buf, no_mgt),
--    4      => kDummyRegion,             -- HighSpeedBus
--    5      => kDummyRegion,             -- PCIe, AXI & TCDS
    6      => (no_mgt, buf, no_fmt, buf, no_mgt),
--    7      => (csp25, buf, no_fmt, buf, csp25),
    8      => (lpgbt, buf, no_fmt, buf, lpgbt),
    9      => (lpgbt, buf, no_fmt, buf, lpgbt),
    10     => (lpgbt, buf, no_fmt, buf, lpgbt),
--    11     => (csp25, buf, no_fmt, buf, csp25),
--    12     => (csp25, buf, no_fmt, buf, csp25),
--    13     => (csp25, buf, no_fmt, buf, csp25),
--    14     => (csp25, buf, no_fmt, buf, csp25),
--    15     => kDummyRegion,             -- Unconnected
--    -- Cross-chip
--    16     => kDummyRegion,             -- Unconnected
--    17     => (csp25, buf, no_fmt, buf, csp25),
--    18     => (csp25, buf, no_fmt, buf, csp25),
--    19     => (csp25, buf, no_fmt, buf, csp25),
--    20     => (csp25, buf, no_fmt, buf, csp25),
--    21     => (csp25, buf, no_fmt, buf, csp25),
--    22     => (csp25, buf, no_fmt, buf, csp25),
--    23     => (csp25, buf, no_fmt, buf, csp25),
--    24     => (csp25, buf, no_fmt, buf, csp25),
--    25     => (csp25, buf, no_fmt, buf, csp25),
--    26     => kDummyRegion,             -- Unconnected
--    27     => kDummyRegion,             -- HighSpeedBus
--    28     => (csp25, buf, no_fmt, buf, csp25),
--    29     => (csp25, buf, no_fmt, buf, csp25),
--    30     => (csp25, buf, no_fmt, buf, csp25),
--    31     => (csp25, buf, no_fmt, buf, csp25),
    others => kDummyRegion
  );

  -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
  constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    8 => (
      0=>(false, true, 0, false, lpgbtv0),
      1=>(false, true, 0, false, lpgbtv0),
      2=>(false, true, 0, false, lpgbtv0),
      3=>(false, true, 0, false, lpgbtv0)
    ),
    9 => (
      0=>(false, true, 0, false, lpgbtv0),
      1=>(false, true, 0, false, lpgbtv0),
      2=>(false, true, 0, false, lpgbtv0),
      3=>(false, true, 0, false, lpgbtv0)
    ),  
    10 => (
      0=>(false, true, 0, false, lpgbtv0),
      1=>(false, true, 0, false, lpgbtv0),
      2=>(false, true, 0, false, lpgbtv0),
      3=>(false, true, 0, false, lpgbtv0)
    ),  
    others => kDummyRegionDataFramer
  );

  -- for lpgbt
  constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := (
    8   => (FEC5,  DATARATE_5G12, PCS),
    9   => (FEC5,  DATARATE_5G12, PCS),
    10  => (FEC5,  DATARATE_5G12, PCS),
    others => kDummyRegionLpgbt
  );

  -- Specify the slink quad using the corresponding region conf ID
  -- Specify slink channels to enable using the channel mask
  constant SLINK_CONF : slink_conf_array_t := (
    others => kNoSlink
  ); 



end emp_project_decl;
-------------------------------------------------------------------------------
