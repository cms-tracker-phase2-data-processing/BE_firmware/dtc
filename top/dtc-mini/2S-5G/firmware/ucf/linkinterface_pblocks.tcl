create_pblock pblock_p_quad_8
resize_pblock pblock_p_quad_8 -add CLOCKREGION_X5Y8:CLOCKREGION_X6Y8
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[0].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[1].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[2].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_8] [get_cells -quiet [list {payload/module[3].LinkInterface}]]

create_pblock pblock_p_quad_9
resize_pblock pblock_p_quad_9 -add CLOCKREGION_X5Y9:CLOCKREGION_X6Y9
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[4].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[5].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[6].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_9] [get_cells -quiet [list {payload/module[7].LinkInterface}]]

create_pblock pblock_p_quad_10
resize_pblock pblock_p_quad_10 -add CLOCKREGION_X5Y10:CLOCKREGION_X6Y10
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[8].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[9].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[10].LinkInterface}]]
add_cells_to_pblock [get_pblocks pblock_p_quad_10] [get_cells -quiet [list {payload/module[11].LinkInterface}]]