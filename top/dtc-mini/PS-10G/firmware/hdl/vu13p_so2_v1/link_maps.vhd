library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    constant cNumberOfFEModules   : integer := 4;
    constant cNumberOfOutputLinks : integer := 4;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (
        0 => (32, "PS", 10, "CIC2"),
        1 => (33, "PS", 10, "CIC2"),
        2 => (34, "PS", 10, "CIC2"),
        3 => (35, "PS", 10, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (
        12, 13, 14, 15
    );

end package dtc_link_maps;
