library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    constant cNumberOfFEModules   : integer := 4;
    constant cNumberOfOutputLinks : integer := 4;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;
    constant cDTCInputLinkMap     : tDTCInputLinkMap := (
        0 => (8,  "2S", 5, "CIC2"),
        1 => (9,  "2S", 5, "CIC2"),
        2 => (10, "PS", 5, "CIC2"),
        3 => (11, "2S", 5, "CIC2")
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap    : tDTCOutputLinkMap := (
        16, 17, 18, 19
    );

end package dtc_link_maps;
