-- emp_device_decl
--
-- Defines constants for the whole device


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.emp_framework_decl.all;


package emp_device_decl is

  constant BOARD_DESIGN_ID : std_logic_vector(7 downto 0) := X"4a";

  constant TCDS2_MGT_TYPE : io_gt_kind_t     := io_gty;
  constant TCDS2_SPEED    : io_tcds2_speed_t := io_tcds2_10g;

  constant N_REGION     : integer := 32;
  constant N_REFCLK     : integer := 30;
  constant CROSS_REGION : integer := 15;

  constant IO_REGION_SPEC : io_region_spec_array_t(0 to N_REGION - 1) := (
    0      => (io_gty, -1, -1),        -- Bank 220 -- Right column
    1      => (io_gty, 0, 15),         -- Bank 221
    2      => (io_gty, 0, 15),         -- Bank 222
    3      => (io_gty, 0, 15),         -- Bank 223
    4      => (io_gty, 1, 16),         -- Bank 224
    5      => (io_gty, 1, 16),         -- Bank 225
    6      => (io_gty, 2, 17),         -- Bank 226
    7      => (io_gty, 2, 17),         -- Bank 227
    8      => (io_gty, 3, 18),         -- Bank 228
    9      => (io_gty, 3, 18),         -- Bank 229
    10     => (io_gty, 4, 19),         -- Bank 230
    11     => (io_gty, 4, 19),         -- Bank 231
    12     => (io_gty, 5, 20),         -- Bank 232
    13     => (io_gty, 5, 20),         -- Bank 233
    14     => (io_gty, 6, 21),         -- Bank 234
    15     => (io_gty, 6, 21),         -- Bank 235
    -- cross chip
    16     => (io_gty, 7, 22),         -- Bank 135 -- Left column
    17     => (io_gty, 7, 22),         -- Bank 134
    18     => (io_gty, 8, 23),         -- Bank 133
    19     => (io_gty, 8, 23),         -- Bank 132
    20     => (io_gty, 9, 24),         -- Bank 131
    21     => (io_gty, 9, 24),         -- Bank 130
    22     => (io_gty, 10, 25),        -- Bank 129
    23     => (io_gty, 10, 25),        -- Bank 128
    24     => (io_gty, 11, 26),        -- Bank 127
    25     => (io_gty, 11, 26),        -- Bank 126
    26     => (io_gty, 12, 27),        -- Bank 125
    27     => (io_gty, 12, 27),        -- Bank 124
    28     => (io_gty, 13, 28),        -- Bank 123
    29     => (io_gty, 13, 28),        -- Bank 122
    30     => (io_gty, 13, 28),        -- Bank 121
    31     => (io_gty, 14, 29),        -- Bank 120
    others => kIONoGTRegion
    );

end emp_device_decl;
-------------------------------------------------------------------------------

