
# PCIe - Bank 220
# RX & TX: channel 3

# PCIe LOC
set_property LOC GTYE4_CHANNEL_X1Y3 [get_cells -hierarchical -filter {NAME =~infra/dma/*GTYE4_CHANNEL_PRIM_INST}]
set_property LOC PCIE40E4_X0Y0 [get_cells -hierarchical -filter {NAME =~infra/dma/*pcie_4_0_pipe_inst/pcie_4_0_e4_inst}]

# Not sure what is best Clock Root
set_property USER_CLOCK_ROOT [get_clock_regions -of_objects [get_sites PCIE40E4_X0Y0]] [get_nets -of_objects [get_pins -hierarchical -filter NAME=~infra/dma/*bufg_gt_sysclk/O]]
set_property USER_CLOCK_ROOT [get_clock_regions -of_objects [get_sites PCIE40E4_X0Y0]] [get_nets -of_objects [get_pins -hierarchical -filter NAME=~*/phy_clk_i/bufg_gt_intclk/O]]
set_property USER_CLOCK_ROOT [get_clock_regions -of_objects [get_sites PCIE40E4_X0Y0]] [get_nets -of_objects [get_pins -hierarchical -filter {NAME=~*/phy_clk_i/bufg_gt_coreclk/O || NAME=~*/phy_clk_i/bufg_gt_userclk/O}]]
