# Site ID (CFG_ADDRESS @ DC)
set_property IOSTANDARD LVCMOS18 [get_ports {site_id[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {site_id[1]}]
set_property PACKAGE_PIN B30 [get_ports {site_id[0]}]
set_property PACKAGE_PIN C30 [get_ports {site_id[1]}]

# PCIe - Bank 220
# REFCLK: refclk1 
set_property PACKAGE_PIN BC11 [get_ports pcie_sys_clk_p]
# resets
set_property IOSTANDARD LVCMOS18 [get_ports pcie_sys_rst]
set_property PACKAGE_PIN BB15 [get_ports pcie_sys_rst]
set_property PULLUP true [get_ports pcie_sys_rst]
set_false_path -from [get_ports pcie_sys_rst]


# EXTERNAL OSCILLATOR
set_property IOSTANDARD LVCMOS18 [get_ports clk125]
set_property PACKAGE_PIN K21     [get_ports clk125]


# EXTERNAL TTC CLOCK -- LVDS02_03  TBC
set_property PACKAGE_PIN AU16           [get_ports ttc_clk_n]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_clk_n]
set_property PACKAGE_PIN AT17           [get_ports ttc_clk_p]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_clk_p]


# EXTERNAL TTC DATA -- LVDS02_01  TBC -  No #1.  Used #2
set_property PACKAGE_PIN AR16           [get_ports ttc_rx_n]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_rx_n]
set_property PACKAGE_PIN AR17           [get_ports ttc_rx_p]
set_property IOSTANDARD  DIFF_HSTL_I_18 [get_ports ttc_rx_p]


# TCDS - Bank 220
# REFCLK: refclk0
set_property PACKAGE_PIN BD13 [get_ports tcds_clk_p]
# RX & TX: channel 2
set_property PACKAGE_PIN BJ15  [get_ports tcds_tx_p]
set_property LOC GTYE4_COMMON_X1Y0 [get_cells -hierarchical -regexp -filter {REF_NAME =~ {.*GT(H|Y)E(3|4)_COMMON.*} && NAME =~ {ttc.*}}]
set_property LOC GTYE4_CHANNEL_X1Y2 [get_cells -hierarchical -regexp -filter {REF_NAME =~ {.*GT(H|Y)E(3|4)_CHANNEL.*} && NAME =~ {ttc.*}}]


# HEARTBEAT LED
set_property IOSTANDARD  LVCMOS18 [get_ports heartbeat_led]
set_property PACKAGE_PIN J18      [get_ports heartbeat_led]
