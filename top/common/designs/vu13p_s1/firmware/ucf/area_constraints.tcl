delete_pblocks -quiet infra ttc clocks payload
#-------------------------------------


# # Constrain TTC
# create_pblock ttc
# resize_pblock [get_pblocks ttc] -add {CLOCKREGION_X1Y0:CLOCKREGION_X4Y0}
# add_cells_to_pblock [get_pblocks ttc] [get_cells -quiet {ttc}]
# remove_cells_from_pblock [get_pblocks ttc] [get_cells -quiet {ttc/*osc_clock}]
# remove_cells_from_pblock [get_pblocks ttc] [get_cells -quiet {ttc/clocks}]


# Parameters
set lLeftQuadWidth 601
set lRightQuadWidth 601

set lClkBounds [get_XY_bounds [get_clock_regions]]
puts "Clock region boundaries ${lClkBounds}"

lassign [create_quad_pblocks $lLeftQuadWidth $lRightQuadWidth] lNumQuads lLeftBoundary lRightBoundary

# Create the quad p-blocks and store the number of blocks created
puts "Created $lNumQuads quads"

for {set lRegId 0} {$lRegId < 16} {incr lRegId} {
    set q [expr 0 + $lRegId]
    set lQuadBlock [get_pblocks quad_R$q] 
    puts "Populating $lQuadBlock with region $lRegId" 
    
    add_cells_to_pblock $lQuadBlock datapath/rgen\[$lRegId\].region
    remove_cells_from_pblock -quiet $lQuadBlock [get_cells -hierarchical -quiet -filter {NAME =~datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/slow_command_ctrl_inst} ]
    constrain_mgts $lRegId $lQuadBlock 1
}

for {set lRegId 16} {$lRegId < 32} {incr lRegId} {
    set q [expr 31 - $lRegId]
    set lQuadBlock [get_pblocks quad_L$q]
    puts "Populating $lQuadBlock with region $lRegId"

    add_cells_to_pblock $lQuadBlock datapath/rgen\[$lRegId\].region
    remove_cells_from_pblock -quiet $lQuadBlock [get_cells -hierarchical -quiet -filter {NAME =~datapath/rgen[*].region/mgt_gen.fe_mgt_gen.fe_mgt_inst/*/slow_command_ctrl_inst} ]
    constrain_mgts $lRegId $lQuadBlock 0
}


# Payload Area assignment

set lPayload [create_pblock payload]
set lPayloadRect [find_rects [get_sites -of [get_clock_regions -f {ROW_INDEX>0}] -f "RPM_X >= $lLeftBoundary && RPM_X <= $lRightBoundary"]]
add_rects_to_pblock $lPayload $lPayloadRect

add_cells_to_pblock [get_pblocks payload] [get_cells -quiet datapath/rgen[*].pgen.*]


# Infra Area assignment

set lInfra [create_pblock infra]
set lInfraRect [find_rects [get_sites -of [get_clock_regions X*Y0 -f {COLUMN_INDEX>1}] ] ]
add_rects_to_pblock $lInfra $lInfraRect
add_cells_to_pblock [get_pblocks infra] [get_cells -quiet infra/*]
