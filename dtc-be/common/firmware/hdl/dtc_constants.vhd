library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


package dtc_constants is

  constant cNumberOfCICs : integer := 2;

  constant cL1DataFifoWidth : integer := 32;
  constant cL1CtrlFifoWidth : integer := 16;

end package dtc_constants;
