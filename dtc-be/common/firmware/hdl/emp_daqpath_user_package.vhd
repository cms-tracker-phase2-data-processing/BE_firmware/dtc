library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


package emp_daqpath_user_package is

  -- DAQPATH settings

  --constant N_CHAN           : integer := 8; -- total number of channnels (commented out cause channels number is calculated in payload with number fo FE modules/CICs constants)
  constant N_CHAN_PER_GROUP : integer := 4; -- number of channels per pipeline group (number of group is then = ceil(N_CHAN/N_CHAN_PER_GROUP))
  constant DW_BYTES         : integer := 4; -- 32 bit words for DTC


end package emp_daqpath_user_package;

