library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  ---
  use work.dtc_constants.all;


package dtc_data_types is

  type tCICHeader is record
    CIC_conf   : std_logic;
    status     : std_logic_vector(8 downto 0);
    bcid       : std_logic_vector(11 downto 0);
    stub_count : std_logic_vector(5 downto 0);
  end record tCICHeader;

  type tCICHeaderArray is array(integer range <>) of tCICHeader;

  function cicHeaderToSLV (header: tCICHeader) return std_logic_vector;


  type tFastCommand is record
    counter_mode  : std_logic;
    fast_reset    : std_logic;
    l1a_trig      : std_logic;
    cal_pulse     : std_logic;
    counter_reset : std_logic;
  end record tFastCommand;

  function fastCommandToSLV (command: tFastCommand) return std_logic_vector;


  type tDaqFlag is record
    data_word : std_logic;
    event_id  : std_logic;
    n_words   : std_logic;
  end record tDaqFlag;

  type tDaqFlagArray is array(integer range 0 to cNumberOfCICs - 1) of tDaqFlag;

  type tDaqFlagLinkArray is array (integer range <>) of tDaqFlagArray;

  constant DaqFlag_NULL      : tDaqFlag                  := ('0', '0', '0');
  constant DaqFlagArray_NULL : tDaqFlagArray             := ((others => DaqFlag_NULL));


  type tDaqData is record
    data_word : std_logic_vector(cL1DataFifoWidth - 1 downto 0);
    event_id  : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);
    n_words   : std_logic_vector(cL1CtrlFifoWidth - 1 downto 0);
  end record tDaqData;

  type tDaqDataArray is array(integer range 0 to cNumberOfCICs - 1) of tDaqData;

  type tDaqDataLinkArray is array (integer range <>) of tDaqDataArray;

  constant DaqData_NULL      : tDaqData                  := ((others => '0'), (others => '0'), (others => '0'));
  constant DaqDataArray_NULL : tDaqDataArray             := ((others => DaqData_NULL));

end package dtc_data_types;


package body dtc_data_types is

  function cicHeaderToSLV (header: tCICHeader) return std_logic_vector is
  begin

    return header.CIC_conf & header.status & header.bcid & header.stub_count;

  end cicHeaderToSLV;

  function fastCommandToSLV (command: tFastCommand) return std_logic_vector is
  begin

    return "000" & command.fast_reset & "000" & command.l1a_trig & "000" & command.cal_pulse & "000" & command.counter_reset;

  end fastCommandToSLV;

end package body dtc_data_types;

