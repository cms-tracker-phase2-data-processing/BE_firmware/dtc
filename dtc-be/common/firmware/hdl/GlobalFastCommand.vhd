library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;

use work.module_constants.all;
use work.front_end_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


entity GlobalFastCommand is
port(
    --- Input Ports ---
    clk_p             : in  std_logic;
    clk40             : in  std_logic;
    ext_fcmd          : in  tFastCommand;
    --- Output Ports ---
    global_fcmd       : out tFastCommand;
    --- IPBus Ports ---
    clk               : in  std_logic;
    rst               : in  std_logic;
    ipb_in            : in  ipb_wbus;
    ipb_out           : out ipb_rbus
);
end GlobalFastCommand;


architecture rtl of GlobalFastCommand is


-- IPBus registers

signal fast_cmd_status          : ipb_reg_v(3 downto 0) := (others => (others => '0'));
signal fast_cmd_ctrl            : ipb_reg_v(3 downto 0) := (others => (others => '0'));

signal source_mask              : std_logic_vector(3 downto 0) := (others => '0');
signal gen_run                  : std_logic := '0';
signal gen_cntr_mode            : std_logic := '0';
signal gen_repeat               : integer range 0 to 1023 := 0;

type tFastCmdBuffer is array(6 downto 0) of std_logic_vector(15 downto 0);
signal gen_buffer               : tFastCmdBuffer := (others => (others => '0'));


-- Global fast command generator

type tGenState is (start, countdown, stop);
signal gen_state                : tGenState := start;

signal gen_instruction          : std_logic_vector(1 downto 0) := (others => '0');
signal gen_counter              : integer range 0 to 1023 := 0;
signal gen_rpt_counter          : integer range 0 to 1023 := 0;
signal cmd_gen                  : tFastCommand;

signal ctr_reset_counter        : integer range 0 to 255 := 0;
signal cal_pulse_counter        : integer range 0 to 255 := 0;
signal l1a_trig_counter         : integer range 0 to 255 := 0;
signal fast_reset_counter       : integer range 0 to 255 := 0;


-- Output

signal cmd_local                : tFastCommand;
signal cmd_out                  : tFastCommand;



begin


--==============================--
fast_cmd: entity work.ipbus_ctrlreg_v
--==============================--
generic map(
    N_CTRL            => 4,
    N_STAT            => 4
)
port map(
    clk               => clk,
    reset             => rst,
    ipbus_in          => ipb_in,
    ipbus_out         => ipb_out,
    d                 => fast_cmd_status,
    q                 => fast_cmd_ctrl
);


--==============================--
-- Generators
--==============================--


--==============================--
fsm: process(clk40)
--==============================--

    variable buf_index: integer range 0 to 6 := 0;

begin

    if rising_edge(clk40) then

        -- latch IPBus control registers in payload clock domain
        source_mask     <= fast_cmd_ctrl(0)(3 downto 0);
        gen_run         <= fast_cmd_ctrl(0)(4);
        gen_cntr_mode   <= fast_cmd_ctrl(0)(5);
        gen_repeat      <= to_integer(unsigned(fast_cmd_ctrl(0)(15 downto 6)));

        -- 16b fast command generator commands : instruction [2b] + wait counter [10b] + fast command [4b]
        -- instructions : 0 = return to first word; 1 = move to next word; 2 = stop at this word; 3 = undefined
        -- fast commands: bit3 = fast_reset; bit2 = l1a_trigger; bit1 = cal_pulse; bit0 = counter_reset
        gen_buffer(0) <= fast_cmd_ctrl(1)(15 downto 0);
        gen_buffer(1) <= fast_cmd_ctrl(1)(31 downto 16);
        gen_buffer(2) <= fast_cmd_ctrl(2)(15 downto 0);
        gen_buffer(3) <= fast_cmd_ctrl(2)(31 downto 16);
        gen_buffer(4) <= fast_cmd_ctrl(3)(15 downto 0);
        gen_buffer(5) <= fast_cmd_ctrl(3)(31 downto 16);
        gen_buffer(6) <= (others => '0');   --null buffer

        -- only run fast command generator when gen_run enabled
        if gen_run = '1' then

            case gen_state is

                -- initial state when gen_run enabled; initialise and send fast command in first word
                when start =>
                   buf_index        := 0;

                   gen_counter     <= to_integer(unsigned(gen_buffer(buf_index)(13 downto 4)));
                   gen_instruction <= gen_buffer(buf_index)(15 downto 14);

                   cmd_gen.fast_reset    <= gen_buffer(buf_index)(3);
                   cmd_gen.l1a_trig      <= gen_buffer(buf_index)(2);
                   cmd_gen.cal_pulse     <= gen_buffer(buf_index)(1);
                   cmd_gen.counter_reset <= gen_buffer(buf_index)(0);

                   gen_state <= countdown;

                -- wait for N clk cycles as defined in current word; then decide on next action
                when countdown =>
                    if gen_counter <= 0 then
                        if gen_instruction = "01" then
                            buf_index := buf_index + 1;  -- move to next word
                        elsif gen_instruction = "00" and gen_rpt_counter > 1 then
                            gen_rpt_counter <= gen_rpt_counter - 1;  -- decrement repeat sequence counter
                            buf_index := 0;  -- return to word 0
                        elsif gen_instruction = "00" and gen_rpt_counter = 0 then
                            buf_index := 0;  -- return to word 0 and repeat ad infinitum
                        else
                            gen_state <= stop;
                            buf_index := 6;  -- instruction indicates stop or is undefined
                        end if;

                        -- set counter, instruction, and fast commands according to next word
                        gen_counter     <= to_integer(unsigned(gen_buffer(buf_index)(13 downto 4)));
                        gen_instruction <= gen_buffer(buf_index)(15 downto 14); 

                        cmd_gen.fast_reset    <= gen_buffer(buf_index)(3);
                        cmd_gen.l1a_trig      <= gen_buffer(buf_index)(2);
                        cmd_gen.cal_pulse     <= gen_buffer(buf_index)(1);
                        cmd_gen.counter_reset <= gen_buffer(buf_index)(0);
                    else
                        gen_counter <= gen_counter - 1;
                        cmd_gen      <= ('0', '0', '0', '0', '0');
                    end if;

                -- final state when buffers end, instruction indicates stop, or undefined sequence
                -- remain here until gen_run is reset
                when others =>
                    cmd_gen <= ('0', '0', '0', '0', '0');

            end case;

            -- update fast counters for debugging when gen_run is enabled
            if (fast_reset_counter < 255 and cmd_gen.fast_reset = '1') then
                fast_reset_counter <= fast_reset_counter + 1;
            end if;

            if (l1a_trig_counter < 255 and cmd_gen.l1a_trig = '1') then
                l1a_trig_counter <= l1a_trig_counter + 1;
            end if;

            if (cal_pulse_counter < 255 and cmd_gen.cal_pulse = '1') then
                cal_pulse_counter <= cal_pulse_counter + 1;
            end if;

            if (ctr_reset_counter < 255 and cmd_gen.counter_reset = '1') then
                ctr_reset_counter <= ctr_reset_counter + 1;
            end if;

        else

            -- reset fsm and counters when local_gen_run is disabled
            gen_state          <= start;
            gen_rpt_counter    <= gen_repeat;
            cmd_gen            <= ('0', '0', '0', '0', '0');
            fast_reset_counter <= 0;
            l1a_trig_counter   <= 0;
            cal_pulse_counter  <= 0;
            ctr_reset_counter  <= 0;

        end if;

    end if;

end process fsm;

-- prepare locally generated fast commands
cmd_local.counter_mode  <= gen_cntr_mode;
cmd_local.fast_reset    <= cmd_gen.fast_reset;
cmd_local.l1a_trig      <= cmd_gen.l1a_trig;
cmd_local.cal_pulse     <= cmd_gen.cal_pulse;
cmd_local.counter_reset <= cmd_gen.counter_reset;

-- prepare IPBus status registers
fast_cmd_status(0)(7 downto 0)   <= std_logic_vector(to_unsigned(ctr_reset_counter, 8));
fast_cmd_status(0)(15 downto 8)  <= std_logic_vector(to_unsigned(cal_pulse_counter, 8));
fast_cmd_status(0)(23 downto 16) <= std_logic_vector(to_unsigned(l1a_trig_counter, 8));
fast_cmd_status(0)(31 downto 24) <= std_logic_vector(to_unsigned(fast_reset_counter, 8));





--==============================--
-- Output : source select & formatting
--==============================--


--==============================--
cmd_select: process(clk40)
--==============================--
begin

    if rising_edge(clk40) then

        -- select between tcds and local fast command sources
        cmd_out.counter_mode  <= cmd_local.counter_mode;
        cmd_out.fast_reset    <= ( ext_fcmd.fast_reset    and source_mask(3) ) or cmd_local.fast_reset;
        cmd_out.l1a_trig      <= ( ext_fcmd.l1a_trig      and source_mask(2) ) or cmd_local.l1a_trig;
        cmd_out.cal_pulse     <= ( ext_fcmd.cal_pulse     and source_mask(1) ) or cmd_local.cal_pulse;
        cmd_out.counter_reset <= ( ext_fcmd.counter_reset and source_mask(0) ) or cmd_local.counter_reset;

    end if;

end process cmd_select;


global_fcmd <= cmd_out;



end rtl;

