import argparse
import os

# Run with python3 in work/proj/
# Converts CIC2 output data from Monte Carlo log file (CIC_OUT_TO_FILE_5G_HYBRID.txt) to EMP format
# Based on EMP pattern generation in l1tk-for-emp project

def parseArguments():
    # Create argument parser
    parser = argparse.ArgumentParser(description="Convert CIC2 output data log into an EMP format file")
 
    # Optional arguments
    parser.add_argument("-d", "--directory", dest="inputDir", help="directory containing input data", type=str, default="../src/dtc/testbench/software/python")
    parser.add_argument("-o","--outFile", dest="outFile", help="output file name", type=str, default="../src/dtc/top/dtc-mini/PS-5G/firmware/hdl/CIC2_EMP.txt")

    # Parse arguments
    args = parser.parse_args()

    return args

# Input file which has data from both the CICs in a hybrid
inFile = {}
inFile[0]  = "CIC_OUT_TO_FILE_5G_HYBRID.txt"

channels = ["q00c0","q00c1","q00c2","q00c3","q01c0","q01c1","q01c2","q01c3","q02c0","q02c1","q02c2","q02c3","q03c0","q03c1","q03c2","q03c3","q04c0","q04c1","q04c2","q04c3"]

empWordLen=16 # Hex chars in an EMP data word
clksInTM=192 # Length of TM period in clock cycles
clksInGap=6  # Gap when no valid data sent.

if __name__ == '__main__':

  args = parseArguments()

  outFile = open(args.outFile, 'w')

  allData = {}
  for iCount, fileName in inFile.items():
    file = open(args.inputDir + "/" + fileName, 'r')
    lines = file.readlines()
    allData[(iCount)] = []
    for line in lines:
      words = line.split()
      data1 = hex(int(words[3],2))
      if len(words) == 4:
        #empData = "1v"+data1[2:].zfill(empWordLen)
        empData = "1v"+"00000000800000"+data1[2:].zfill(2)
      else:
        data2 = hex(int(words[4],2))
        #empData = "1v"+data2[2:].zfill(int(empWordLen/2))+data1[2:].zfill(int(empWordLen/2))
        empData = "1v"+"800000"+data2[2:].zfill(2)+"800000"+data1[2:].zfill(2)
      allData[(iCount)].append(empData)

  outFile.write("Board x0\n")
  outFile.write(" Quad/Chan :        ") 
  for iCount in inFile:
    outFile.write("%s              " %(channels[iCount+8]))
  outFile.write("\n")
  outFile.write("      Link :         ")
  for iCount in inFile:
    outFile.write("%s                " %(str(iCount+8).zfill(3)))
  outFile.write("\n")

  gapData  = "0v0000000000000000"
  nullData = "1v0000000000000000"
  iClk = 0
  for iFrame in range(0,clksInTM):
    iFrameCorr = iFrame - clksInGap
    outFile.write("Frame %s : " %(str(iClk).zfill(4)))
    iClk += 1
    for iCount in inFile:
      theKey = (iCount)
      empDataList = allData[theKey]
      if (iFrame < clksInGap):
        outFile.write("%s " %gapData)
      elif (iFrameCorr < len(empDataList)):
        outFile.write("%s " %empDataList[iFrameCorr])
      else:
        outFile.write("%s " %nullData)
    outFile.write("\n")

  print("Output written to file ",args.outFile)
