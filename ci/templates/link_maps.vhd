library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.dtc_link_maps_func.all;

package dtc_link_maps is

    {{ cNumberOfFEModules }}

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of tDTCInputLinkConfig;

    {{ cDTCInputLinkMap }}

    {{ cNumberOfOutputLinks }}

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;

    {{ cDTCOutputLinkMap }}

end package dtc_link_maps;
