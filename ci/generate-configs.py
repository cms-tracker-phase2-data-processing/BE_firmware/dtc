from yaml import safe_load, safe_dump
from copy import deepcopy
import os


job_template = {
    "stage": "Launch Pipeline",
    "trigger": {
        "include": "ci/.launch_vivado.yml",
        "strategy": "depend"
    },
    "variables": {}
}


def main():
    # Open config file and load into dict
    with open("./build_configs.yml") as f:
        build_configs = safe_load(f)
    
    # Base configuration dict that will become YAML file
    pipeline_config = {
        "stages": ["Launch Pipeline"]
    }
    
    # Create job configuration for each config in the config file
    for config in build_configs:
        job = deepcopy(job_template)
        
        # Set pipeline name, adding detail based on whether it uses the database to generate files
        if "generate_files" in build_configs[config] and build_configs[config]["generate_files"] is True:
            job["variables"]["PIPELINE_NAME"] = f"Deployment: {config}"
        else:
            job["variables"]["PIPELINE_NAME"] = f"Test Build: {config}"

        job["variables"]["PROJECT_ALGORITHM"] = build_configs[config]["algorithm"]
        job["variables"]["PROJECT_DEPFILE"] = build_configs[config]["dep_file"]
        
        # Set the EMPHub tag based on whether the pipeline is scheduled or not
        if os.getenv("CI_PIPELINE_SOURCE") == "schedule":
            job["variables"]["EMPHUB_TAG"] = f"{config}_{os.getenv('SCHEDULE_REF_NAME')}"
        else:
            job["variables"]["EMPHUB_TAG"] = f"{config}_{os.getenv('CI_COMMIT_REF_NAME')}-{os.getenv('CI_COMMIT_SHORT_SHA')}"
        
        # If the 'when' tag is included in the config, propagate its value to the pipeline, when pushing or triggering through the web interface
        if "when" in build_configs[config]:
            match os.getenv("CI_PIPELINE_SOURCE"):
                case "push":
                    if os.getenv("CI_OPEN_MERGE_REQUESTS") is None:
                        job["when"] = build_configs[config]["when"]
                case "web":
                    job["when"] = build_configs[config]["when"]
                case _:
                    pass
        
        # Use pipeline name as tag in YAML file
        pipeline_config[job["variables"]["PIPELINE_NAME"]] = job
    
    # Write the YAML
    with open("./configure-ci.yml", "w") as f:
        safe_dump(pipeline_config, f)


if __name__ == "__main__":
    main()