# Code style guidelines

#### Name conventions:
- Constants should have a `c` prefix and be in camel case
- Types should have a `t` prefix and be in came case
- Entity names should be in camel case, beginning with a capital letter. They should also end with `Instance` and be descriptive
- Processes should be named, beginning with a lowercase `p` and be descriptive
- Generate statements should be named, beginning with `gen` and be descriptive

#### File structure
- Use entities in place of components
- Inputs and outputs of an entity should be grouped under a specific header. IPbus ports should be placed under their own header
- Any new block should be indented relative to its parent (including architectures, generate statements, processes and if statements).
