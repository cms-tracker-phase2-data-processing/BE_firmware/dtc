[![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/commits/master)
---
[![FFs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/FFs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![LUTRAMs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/LUTRAMs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![DSP Blocks](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/DSP%20Blocks.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![Logic LUTs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/Logic%20LUTs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![RAMB18](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/RAMB18.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![RAMB36](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/RAMB36.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![SRLs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/SRLs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![Total LUTs](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/Total%20LUTs.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
[![URAM](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/-/jobs/artifacts/master/raw/ci/badges/URAM.svg?job=Implement)](https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc/)
# DTC
Repository for the Outer Tracker DTC for the CMS experiment at CERN


## Contents
[[_TOC_]]


### Local build instructions
The best way to build and validate the firmware is via the CI!  But for development and debugging it might be sometimes more convenient to build locally.  In order to do this, check out the pre-requisite packages below and then follow the instructions to build the project with ipbb, or use the Vivado GUI.

#### Pre-requisites

*Note: You need to be a member of the `cms-tracker-phase2-backend-development` egroup in order to clone the `emp-fwk` repository. In order to add yourself to that egroup, go to the "Members" tab of [this page](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10263144), and click on the "Add me" button; you may need to wait ~ 24 hours to get access to the GitLab repo*

*Note: You need to be a member of the `cms-tcds2-users` egroup in order to clone the `cms-tcds2-firmware` repository. In order to add yourself to that egroup, go to the "Members" tab of [this page](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10380295), and click on the "Add me" button; you may need to wait ~ 24 hours to get access to the GitLab repo*

##### Setup steps
Download and source ipbb the [IPbus Builder Tool](https://github.com/ipbus/ipbb), and initialise the environment.
```
curl -L https://github.com/ipbus/ipbb/archive/dev/2021j.tar.gz | tar xvz
source ipbb-dev-2021j/env.sh

kinit your_user_name@CERN.CH

ipbb init working_dir
cd working_dir
```
##### Clone common packages
Clone all packages necessary for the DTC project to build.
```
ipbb add git https://:@gitlab.cern.ch:8443/p2-xware/firmware/emp-fwk.git -b feature/daqpath_v2
ipbb add git https://gitlab.cern.ch/ttc/legacy_ttc.git -b v2.1
ipbb add git https://:@gitlab.cern.ch:8443/cms-tcds/cms-tcds2-firmware.git -b v0_1_1
ipbb add git https://gitlab.cern.ch/HPTD/tclink.git -r fda0bcf
ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.9
ipbb add git https://gitlab.cern.ch/gbt-fpga/gbt-fpga.git -b gbt_fpga_6_1_0
ipbb add git https://gitlab.cern.ch/gbt-fpga/lpgbt-fpga.git -b v.2.1
ipbb add git https://:@gitlab.cern.ch:8443/gbtsc-fpga-support/gbt-sc.git -b gbt_sc_4_1
```
##### Clone DTC specific packages!
```
ipbb add git https://gitlab.cern.ch/dmonk-emp-projects/histogram.git
ipbb add git https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc.git -b feature/daqpath_tb
ipbb add git https://gitlab.cern.ch/cms-tracker-phase2-data-processing/BE_firmware/dtc-stub-processing.git
```
##### Create an example project

(DTC project with daqpath is available only with dtc-mini so far.)

First create a project area by sourcing the desired project dep file.  For example, for the 2S DTC-mini project on a KU15P,
```
ipbb proj create vivado my_dtc_test dtc:top/dtc-mini/2S-5G ku15p_sm1_v1/top.dep
cd proj/my_dtc_test
```
and similarly, for the PS 5G DTC-mini project on a KU15P,
```
ipbb proj create vivado my_dtc_test dtc:top/dtc-mini/PS-5G ku15p_sm1_v1/top.dep
cd proj/my_dtc_test
```
*Note: For the following commands (not always necessary), you need to ensure that can find & use the `gen_ipbus_addr_decode` script - e.g. for a standard uHAL installation*
```
export PATH=/opt/cactus/bin/uhal/tools:$PATH LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
ipbb ipbus gendecoders
```
Finally, generate the project
```
ipbb vivado generate-project --single
```
The generated `.xpr` project file can be opened with Vivado if desired.

#### Build and package firmware
To use ipbb to build the firmware and package the bitfile with corresponding address tables run,
```
ipbb vivado synth -j4 impl -j4
ipbb vivado package
```

### Input
Data is received at the DTC from 72 lpGBT links from FE modules on the detector. The format for the packets sent over the link can be found in the [spec doc](https://trello-attachments.s3.amazonaws.com/595a8b0c5f35035b315efbba/5e66098f684645843c9c3a43/d0e9d527f162d9371c3cbcc811e09925/CIC2_specs_v1p0.pdf).

### Front End Processing
The link decoding stage of the DTC consists of both a framer within the datapath and a set of entities in the payload for each input link. The framer converts the wide xGBT word (112 bits at 40MHz) to a stream of 10-12 bit words (5-6 bits per CIC) sent over 8 clock cycles per 40MHz.

The two CIC data streams are individually processed to extract the stubs, then combined into a single stream to be sent to the Back End Processing.

#### Pre-conversion format
The first 18 bits represent the stub information, followed by the header for the boxcar.
Type: lword
| Name                | Index range    | Length |
| ---                 | ---            | ---    |
| Bend                | `3 downto 0`   | 4      |
| Stub address        | `11 downto 4`  | 8      |
| Chip ID             | `14 downto 12` | 3      |
| Timing offset       | `17 downto 15` | 3      |
| Boxcar multiplicity | `23 downto 18` | 6      |
| Bx ID               | `35 downto 24` | 12     |
| Status              | `34 downto 36` | 9      |
| FE type             | `35`           | 1      |

#### Post-conversion format
Type: lword
| Name   | Index range    | Length |
| ---    | ---            | ---    |
| Bx     | `6 downto 0`   | 7      |
| Row    | `17 downto 7`  | 11     |
| Column | `22 downto 18` | 5      |
| Bend   | `29 downto 23` | 6      |


### Back End Routing

#### Formats

##### Constants
| Name         | Type    | Value | Description |
| ---          | ---     | ---   | ---         |
| `TMPtfp`     | integer | 18    | time multiplex period of track finding processor |
| `widthBend`  | integer | 6     | Number of bits allocated for the bend |
| `widthBX`    | integer | 7     | Number of bits allocated for the bunch crossing ID |
| `widthCol`   | integer | 5     | Number of bits allocated for the column value |
| `widthRow`   | integer | 11    | Number of bits allocated for the row value |
| `widthTMPfe` | integer | 3     | Number of bits allocated for the time multiplex period of the FE modules |



##### t_stubFE
Type: record

Used as an interface format between the FormatInput and Transform.

| Entry   | Data Type                                  | Length |
| ---     | ---                                        | ---    |
| `reset` | `std_logic`                                | 1      |
| `valid` | `std_logic`                                | 1      |
| `bx`    | `std_logic_vector(widthBX - 1 downto 0)`   | 7      |
| `row`   | `std_logic_vector(widthRow - 1 downto 0)`  | 11     |
| `col`   | `std_logic_vector(widthCol - 1 downto 0)`  | 5      |
| `bend`  | `std_logic_vector(widthBend - 1 downto 0)` | 6      |
