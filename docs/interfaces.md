# DTC Interfaces

[[_TOC_]]

## Input to DTC Processor

<table class="tg">
<thead>
  <tr>
    <th class="tg-20o4">Section</th>
    <th class="tg-20o4">Bit Range</th>
    <th class="tg-20o4">Bit Breakdown</th>
    <th class="tg-20o4">Range</th>
    <th class="tg-20o4">Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-exjp" rowspan="6">Stub CIC 0</td>
    <td class="tg-exjp" rowspan="6">0 - 31</td>
    <td class="tg-ng7p">1 : 0</td>
    <td class="tg-ng7p">0 to 1</td>
    <td class="tg-ng7p">Valid</td>
  </tr>
  <tr>
    <td class="tg-ng7p">7 : 1 - 7</td>
    <td class="tg-ng7p">0 to 71</td>
    <td class="tg-ng7p">Bx</td>
  </tr>
  <tr>
    <td class="tg-ng7p">11 : 8 - 18</td>
    <td class="tg-ng7p">-1024 to 1023</td>
    <td class="tg-ng7p">Row</td>
  </tr>
  <tr>
    <td class="tg-ng7p">4 : 19 - 22</td>
    <td class="tg-ng7p">-8 to 7</td>
    <td class="tg-ng7p">Column</td>
  </tr>
  <tr>
    <td class="tg-ng7p">4 : 23 - 26</td>
    <td class="tg-ng7p">-8 to 7</td>
    <td class="tg-ng7p">Bend</td>
  </tr>
  <tr>
    <td class="tg-ng7p">5 : 27 - 32</td>
    <td class="tg-ng7p">N/A</td>
    <td class="tg-ng7p">Not used</td>
  </tr>
  <tr>
    <td class="tg-exjp" rowspan="6">Stub CIC 1</td>
    <td class="tg-exjp" rowspan="6">32 - 63</td>
    <td class="tg-ng7p">1 : 0</td>
    <td class="tg-ng7p">0 to 1</td>
    <td class="tg-ng7p">Valid</td>
  </tr>
  <tr>
    <td class="tg-ng7p">7 : 1 - 7</td>
    <td class="tg-ng7p">0 to 71</td>
    <td class="tg-ng7p">Bx</td>
  </tr>
  <tr>
    <td class="tg-ng7p">11 : 8 - 18</td>
    <td class="tg-ng7p">-1024 to 1023</td>
    <td class="tg-ng7p">Row</td>
  </tr>
  <tr>
    <td class="tg-ng7p">4 : 19 - 22</td>
    <td class="tg-ng7p">-16 to 15</td>
    <td class="tg-ng7p">Column</td>
  </tr>
  <tr>
    <td class="tg-ng7p">4 : 23 - 26</td>
    <td class="tg-ng7p">-8 to 7</td>
    <td class="tg-ng7p">Bend</td>
  </tr>
  <tr>
    <td class="tg-ng7p">5 : 27 - 32</td>
    <td class="tg-ng7p">N/A</td>
    <td class="tg-ng7p">Not used</td>
  </tr>
</tbody>
</table>

Max. 132 stubs per boxcar (8 BX, 360 MHz), 6 frames between boxcars reserved (header, ...)

## Input to DTC Formatter

2 CICs per link successively (i.e. stubs from second CIC after stubs from first CIC) 

<table class="tg">
<thead>
  <tr>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Section</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Bit range</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Bit breakdown</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Range</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Description</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-y0hg" rowspan="6"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Module Stub</span></td>
    <td class="tg-y0hg" rowspan="6"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">1  : 0</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 1</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Stub Valid</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">7  : 1 - 7</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 71</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Bx</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">11: 8 - 18</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">-1024 to 1023</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Row</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">5  : 19 - 23</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">-16 to 15</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Column</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">4  : 24 - 27</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">-8 to 7</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Bend</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">36 : 28 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">N/A</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Not Used</span></td>
  </tr>
</tbody>
</table>

Max. 66 stubs per boxcar (8 BX, 360 MHz), 6 frames between boxcars reserved (header, ...)

## Input to DTC Router

2 lwords needed to transport 71 bit per clk (64 bit stub data + 7 bit routing data)

<table class="tg">
<thead>
  <tr>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Section</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Bit range</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Bit breakdown</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Range</span></th>
    <th class="tg-b3sw"><span style="font-weight:700;font-style:normal;text-decoration:none;color:#000">Description</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-ycr8" rowspan="3"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">lword 0</span></td>
    <td class="tg-ycr8" rowspan="3"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">7  : 0 - 6</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 71</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Bx</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">2  : 7 - 8</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">N/A</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Nonant pattern</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">57: 9 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">N/A</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Not Used</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8" rowspan="10"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">lword 1</span></td>
    <td class="tg-ycr8" rowspan="10"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">0 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">1  : 0</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Stub Valid</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">12: 1 - 12</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">120 cm</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">R</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">12: 13 - 24</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">240 cm</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Z</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">17: 25 - 41</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">1 rad</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Phi</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">4  : 42 - 45</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Alpha</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">4  : 46 - 49</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Bend</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">2  : 50 - 51</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Layer</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">1  : 52</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Barrel</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">1  : 53</span></td>
    <td class="tg-ycr8"></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">PS Module</span></td>
  </tr>
  <tr>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">10  : 54 - 63</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">N/A</span></td>
    <td class="tg-ycr8"><span style="font-weight:400;font-style:normal;text-decoration:none;color:#000">Not Used</span></td>
  </tr>
</tbody>
</table>

## Outout from DTC Processer to TF

Taken from the [twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HybridDataFormat)

NOTE: the MSB is in the first row and the LSB in the last row. All signed, digitized numbers are "twos compliment".

<table class="tg">
<thead>
  <tr>
    <th class="tg-b3sw">Quantity </th>
    <th class="tg-b3sw">Layer 1-3 (PS) </th>
    <th class="tg-b3sw">Layer 4-6 (2S) </th>
    <th class="tg-b3sw">Disk PS </th>
    <th class="tg-b3sw">Disk 2S </th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-ycr8">r </td>
    <td class="tg-ycr8">7 </td>
    <td class="tg-ycr8">7 </td>
    <td class="tg-ycr8">12 ( leading 3 bits not '000') </td>
    <td class="tg-ycr8">7 ('000' + 4) </td>
  </tr>
  <tr>
    <td class="tg-ycr8">z </td>
    <td class="tg-ycr8">12 </td>
    <td class="tg-ycr8">8 </td>
    <td class="tg-ycr8">7 </td>
    <td class="tg-ycr8">7 </td>
  </tr>
  <tr>
    <td class="tg-ycr8">φ </td>
    <td class="tg-ycr8">14 </td>
    <td class="tg-ycr8">17 </td>
    <td class="tg-ycr8">14 </td>
    <td class="tg-ycr8">14 </td>
  </tr>
  <tr>
    <td class="tg-ycr8">α </td>
    <td class="tg-ycr8">- </td>
    <td class="tg-ycr8">- </td>
    <td class="tg-ycr8">- </td>
    <td class="tg-ycr8">4 </td>
  </tr>
  <tr>
    <td class="tg-ycr8">bend </td>
    <td class="tg-ycr8">3 </td>
    <td class="tg-ycr8">4 </td>
    <td class="tg-ycr8">3 </td>
    <td class="tg-ycr8">4 </td>
  </tr>
  <tr>
    <td class="tg-ycr8">layer ID </td>
    <td class="tg-ycr8">2 </td>
    <td class="tg-ycr8">2 </td>
    <td class="tg-ycr8">2 </td>
    <td class="tg-ycr8">2 </td>
  </tr>
  <tr>
    <td class="tg-ycr8">valid </td>
    <td class="tg-ycr8">1 </td>
    <td class="tg-ycr8">1 </td>
    <td class="tg-ycr8">1 </td>
    <td class="tg-ycr8">1 </td>
  </tr>
  <tr>
    <td class="tg-6v43">TOTAL </td>
    <td class="tg-6v43">36 + 3 </td>
    <td class="tg-6v43">36 + 3 </td>
    <td class="tg-6v43">36 + 3 </td>
    <td class="tg-6v43">36 + 3</td>
  </tr>
</tbody>
</table>
